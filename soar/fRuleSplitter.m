function xObservable = fRuleSplitter(System,xObservable)

% -------------------------------------------------------------------------
% SOAR toolbox - fRuleSplitter.m
%
% This function labels variables as observable on the basis of splitter
% nodes, e.g. for any pair of streams (a,b) connected to the same splitter node,
% it holds for every component i that x_a,i == x_b,i.
%
% Syntax: 	xObservable = fRuleSplitter(System,xObservable)
%
%	Inputs:
%		System          System description, including graph representation
%		xObservable     Current matrix with observability labels (see
%                       fGenObs.m for details)
%
%	Outputs:
%		xObservable     Updated matrix with observability labels (see
%                       fGenObs.m for details)
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

iit = [System.varindex.ii System.varindex.t ];
xArcSplitter = System.xArcSplitter ;
xObservableIntensive = xObservable(:,iit) ;

if any(xObservableIntensive(:)==1)
    for s=1:size(xArcSplitter,2)

        ObsComponent	=	xObservableIntensive(xArcSplitter(:,s),:);
        NanPattern      =	ObsComponent;
        NanPattern(~isnan(NanPattern))      =   1                           ;

        ObsComponent(isnan(NanPattern))     =   1                           ;
        ObsComponent(:,any(ObsComponent,1))	=   1                           ;
        ObsComponent                        =   ObsComponent.*NanPattern	;

        xObservable(xArcSplitter(:,s),iit)	=	ObsComponent                ;
    end
end
