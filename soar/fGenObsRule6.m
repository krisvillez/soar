function xObservable = fGenObsRule6(System,xObservable,verbose)

% -------------------------------------------------------------------------
% SOAR toolbox - fGenObsRule6.m
%
% This function implements rule 6 of the GENOBS* algorithm [1-3].
%
% Syntax: 	xObservable = fGenObsRule6(System,xObservable,verbose)
%
%	Inputs:
%		System          System description, including graph representation
%       verbose         Level of verbosity [0-4]
%		xObservable     Current matrix with observability labels (see
%                       fGenObs.m for details)
%
%	Outputs:
%		xObservable     Updated matrix with observability labels (see
%                       fGenObs.m for details)
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
% [2] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and
% redundancy classification in generalized process networks - II.
% Algorithms. Comp. Chem. Eng., 12(7), 689-703.
% [3] Villez, K. (2019). GENOBS* and GENRED* algorithms for observability
% and redundancy labeling.  (TR-008-01-0). Technical Report, Eawag,
% Duebendorf, Switzerland.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------



if verbose>=2,  disp('========================================================================================='),  end
if verbose>=1,  disp('	OBSERVABILITY RULE 6 '),    end
if verbose>=2,  disp('========================================================================================='),  end

xMassFlowCandidate	=	and(xObservable(:,System.varindex.m)==0,System.xArcPhysical);

% Find cutsets that have edges with flows that could be labeled
% observable:
Acut                =	System.Acut(~all(System.Acut(:,xMassFlowCandidate)==0,2),:)            ;

% Organize cutsets in order of number of edges and number of
% candidate edges for labelling:
[ ~,index]          =   sortrows([ sum(Acut~=0,2)  -sum(Acut(:,xMassFlowCandidate)==1,2) ] )   ;
Acutsort            =	Acut(index,:)                                                           ;

% Initialization
xMadeChange         =	false       ;

% Loop over all cutsets until there is some flow labelled
% observable or all cutsets have been evaluated:
iCut = 0 ;
nCut = size(Acutsort,1) ;

m = System.varindex.m;
i = System.varindex.ii(1);
n = System.varindex.nn(1);

global OneComponent
while ~xMadeChange && iCut<nCut

    iCut = iCut +1 ;
    if verbose>=4,	disp(['    Cut: ' num2str(iCut) ' of ' num2str(nCut) ]), end

    %     if special % Single-component system

    % Select cutset and keep track of cutsets
    a_              =	Acutsort(iCut,:)       ;
    xInCut0 = a_(:)~=0 ;

    if OneComponent
        if any(xObservable(a_~=0,m)==-1) % no unobservable mass flows
        else % no unobservable mass flows

            xCandidate  = all([xInCut0 xObservable(:,m)==0 xObservable(:,n)~=1 ],2) ;
            %         xCandidate'


            if all( [ sum(xCandidate)==2 sum(System.xArcEqualComposition(xCandidate,:)==1,1)<=1 all(xObservable(xCandidate,i)==1) ],2)
                % 1. exactly 2 mass flow candidates
                % 2. none of the candidate mass flows belong to edges with the same composition
                % 3. all concentrations of candidate streams are observable
                % -> conditions 1-3 are conditions for candidate streams

                xKnown      = all([xInCut0 xObservable(:,m)==1 or(xObservable(:,n)==1,and(xObservable(:,m)==1,xObservable(:,i)==1)) ],2) ;
                if all(xKnown(and(~xCandidate,xInCut0)))
                    xObservable(xCandidate,m) = 1;
                    xMadeChange=1;
                end

            end
        end

    else

        if isfield(System,'symbolic')
            % Define symbolic linear equation system: C_ * u == b_
            [xModalityIncluded,estimatedflows,C_,b_] = fProcedureSystem(System,xObservable,a_,verbose) ;

            % Analyze equation system
            if ~isempty(estimatedflows) && size(C_,1)>=2

                if verbose>=4,	disp( '         Execute procedure TEST'	),	end

                [xMadeChange,xObservableNew] = fProcedureTest(System,estimatedflows,xMadeChange,xModalityIncluded,xObservable,C_,b_,verbose);
                xObservable = xObservableNew;
                %
                %             if xMadeChange
                %                 estimatedflows
                %                 pause
                %             end
            end
        else
            switch System.strName
                case 'KretsovalisMah1988b'
                    if verbose>=4,
                        disp( '         Symbolic Math Toolbox not available yet needed')
                        disp( '         Providing hard-coded results of procedures SYSTEM and TEST instead'	),
                    end
                    xObservable([6 9],m) = 1;
                    xMadeChange = true;
%                 case 'KretsovalisMah1987'
%                     if verbose>=4,
%                         disp( '         Symbolic Math Toolbox not available yet needed')
%                         disp( '         Providing hard-coded results of procedures SYSTEM and TEST instead'	),
%                     end
% %                     xObservable([2 4],m) = 1;
%                     xMadeChange = true;
                otherwise
                    error('Symbolic Math Toolbox not available yet needed')
            end
        end

    end
end

if ~xMadeChange && iCut==nCut
    % set all remaining variables to non-observable
    xObservable(xObservable==0)=-1;
end
