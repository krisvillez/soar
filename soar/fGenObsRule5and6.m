function xObservable = fGenObsRule5and6(System,verbose,xObservable)

% -------------------------------------------------------------------------
% SOAR toolbox - fGenObsRule5and6.m
%
% This function implements rule 5 and 6 of the GENOBS* algorithm [1-3].
%
% Syntax: 	xObservable = fGenObsRule5and6(System,verbose,xObservable)
%
%	Inputs:
%		System          System description, including graph representation
%       verbose         Level of verbosity [0-4]
%		xObservable     Current matrix with observability labels (see
%                       fGenObs.m for details)
%
%	Outputs:
%		xObservable     Updated matrix with observability labels (see
%                       fGenObs.m for details)
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
% [2] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and
% redundancy classification in generalized process networks - II.
% Algorithms. Comp. Chem. Eng., 12(7), 689-703.
% [3] Villez, K. (2019). GENOBS* and GENRED* algorithms for observability
% and redundancy labeling.  (TR-008-01-0). Technical Report, Eawag,
% Duebendorf, Switzerland.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------


if verbose>=2,  disp('========================================================================================='),  end
if verbose>=1,  disp('	OBSERVABILITY RULE 5 '),    end
if verbose>=2,  disp('========================================================================================='),  end

% Cycles
CyclesNoEnergy = System.CyclesAll(:,all(~System.CyclesAll(System.xArcEnergy,:),1));
CyclesNoEnergyNoObservableMassFlow = CyclesNoEnergy(:,all(~CyclesNoEnergy(xObservable(:,System.varindex.m)==1,:),1)) ;

xEmpty = ~any(any(CyclesNoEnergyNoObservableMassFlow(xObservable(:,System.varindex.m)~=1,:),1),2) ;

if xEmpty

    if verbose>=3,	xObservability_old =xObservable; end
    xObservable(xObservable==0)=-1;
    if verbose>=3,	fDisplayLabelChanges(System,xObservability_old,xObservable),	end

else

    if verbose>=3,	xObservability_old =xObservable; end
    if verbose>=2,	disp('	Some variables remain unlabelled. '); end
    xObservable = fGenObsRule6(System,xObservable,verbose) ;
    if verbose>=3,	fDisplayLabelChanges(System,xObservability_old,xObservable),	end

end
