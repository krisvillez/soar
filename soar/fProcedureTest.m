function [xMadeChange,xObservability] = fProcedureTest(System,estimatedflows,xMadeChange,xModalityIncluded,xObservability,C_,b_,verbose)

% -------------------------------------------------------------------------
% SOAR toolbox - fProcedureSystem.m
%
% This function implements the procedure SYSTEM in rule 6 of the GENOBS*
% algorithm [1-3]. 
%
% Syntax: 	[xMadeChange,xObservability] = 
%           fProcedureTest( System,estimatedflows,xMadeChange, ...
%                           xModalityIncluded,xObservability,C_,b_,verbose)
%                           
%	Inputs: 	
%		System              System description, including graph
%                           representation 
%       estimatedflows      Boolean vector indicating which flow rates may
%                           be observable  
%       xMadeChange         Boolean scalar indicating whether a change in
%                           labels has been applied
%		xModalityIncluded	Boolean vector indicating which balance
%                           equations are included
%       xObservability      Current matrix with observability labels (see
%                           fGenObs.m for details)  
%       C_                  Constraint matrix (left-hand side)
%       b_                  Contraint vector (right-hand side)
%       verbose             Level of verbosity [0-4]
%
%	Outputs: 	
%       xMadeChange         Boolean scalar indicating whether a change in
%                           labels has been applied 
%       xObservability      Updated matrix with observability labels (see 
%                           fGenObs.m for details) 
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2019-07-24
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2019 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


m = System.varindex.m ;
iNumberOfComponent=System.iNumberOfComponent;

xStep1to4_converged = false;
%iModality = find(xModalityIncluded(1:(iNumberOfComponent))) ;
iModality = find(xModalityIncluded(1:(iNumberOfComponent+1))) ;

while ~xStep1to4_converged
    
    xStep1to3_converged = false;
    while ~xStep1to3_converged
        
        [nRow,nCol]=size(C_);
        xMarkRow = false(nRow,1);
        xMarkCol = false(nCol,1);
        
        [~,row]=max(sum(logical(C_~=0),2));
        xMarkRow(row) =1;
        xMarkCol(logical(C_(row,:)~=0)) =1;
        
        xCriterion1 = any(logical(C_(:,xMarkCol)~=0),2) ;
       xCriterion2 =  all(~logical(C_(:,~xMarkCol)~=0),2) ;
        row = and(xCriterion1,xCriterion2);
        xMarkRow(row)= 1;
        
        if sum(xMarkCol)>sum(xMarkRow)
            C_ = C_(2:end,:);
            b_ = b_(2:end,:);
            iModality =iModality(2:end,:) ;
        else
            xStep1to3_converged = true;
        end
    end
    
    C_til = C_(xMarkRow,xMarkCol);
    b_til = b_(xMarkRow);
    
    s = size(C_til,2);
    p = length(iModality);
    
    if all(b_til==0) % underdetermined system
        if verbose>4, disp('            zeros'), end
        xDeterminedSystem = false;
    elseif s>=(p+2) % underdetermined system
        if verbose>4, disp('            underdetermined 1'), end
        xDeterminedSystem = false;
    elseif xModalityIncluded(end-1)
        if s>=(p+1) % underdetermined system
            if verbose>4, disp('            underdetermined 2'), end
            xDeterminedSystem = false;
        else
            if verbose>4, disp('            determined or overdetermined'), end
            xDeterminedSystem = true;
        end
    elseif ~and(rank(C_til)==s,rank([C_til b_til])==s)  % overdetermined/underdetermined system
        if verbose>4, disp('            rank check fails'), end
        xDeterminedSystem = false;
    else % perfectly determined system
        if verbose>4, disp('            just determined'), end
        xDeterminedSystem = true;
    end
    
    if ~xDeterminedSystem
        if verbose>4, disp(['           Procedure TEST failed: underdetermined or overdetermined system.' ]), end
        C_ = C_(2:end,:);
        b_ = b_(2:end,:);
        iModality =iModality(2:end,:) ;
    else
        if verbose>4, disp(['           Procedure TEST successful' ]), end
        if verbose>4, disp(['               Observable rates: ' num2str(estimatedflows(xMarkCol)') ]), end
        if xObservability(estimatedflows(xMarkCol),m)~=1
            xMadeChange = true;
        end
        xObservability(estimatedflows(xMarkCol),m)= 1;
        estimatedflows = estimatedflows(~xMarkCol);
        C_ = C_(~xMarkRow,~xMarkCol);
        b_ = b_(~xMarkRow);
        iModality =iModality(~xMarkRow(1:length(iModality)),:) ;
    end
    
    if xMadeChange || any(size(C_)==0)
        if verbose>=4, disp(['           Procedure TEST done' ]), end
        xStep1to4_converged = true;
    end
end



