function iTree = fParetoSearch(fbnd,iLayoutBnd,verbose,visual,strProblem)

% -------------------------------------------------------------------------
% SOAR toolbox - fParetoSearch.m
%
% This function executes a branch-and-bound search for the Pareto front of
% sensor layouts.
%
% Syntax: iTree = fParetoSearch(fbnd,iLayoutBnd,verbose,visual,strProblem)
%
%	Inputs:
%		fbnd        Anonymous function computing the bounds to the
%                   objective functions
%       iLayoutBnd	Indices of two sensor layouts.
%       verbose     Level of verbosity [0-4]
%       visual      Integer indicating the update interval for the
%                   visualization of the Pareto front (number of
%                   iterations; 0 means no plot)
%       strProblem  Name of the problem instance - for the purpose of
%                   command line output
%
%	Outputs:
%       iTree       List of Pareto optimal solutions with the following
%                   columns (in this order):
%                   -	Layout index
%                   -	Layout index +1
%                   -	Lower bound values (number of columns matching
%                       the first output of anonymous function fbnd)
%                   -	Upper bound values (number of columns matching
%                       the first output of anonymous function fbnd)
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

[LB,UB]=fbnd(iLayoutBnd);
nCrit = length(LB);

iTree = nan(100000,2+nCrit*2) ;
iTree(1,:) = [ iLayoutBnd LB(:)' UB(:)' ];
nBranch =1;

iNumIter = 0 ;
iNumIterMax = inf ;
Terminated = false ;

Volume0 = iLayoutBnd(:,2)-iLayoutBnd(:,1);

nBranchMax = 0 ;

CritSeries = 1:nCrit;
BranchSelectMethod = 'maxlowbound';

while ~Terminated && iNumIter<iNumIterMax

    iNumIter = iNumIter+1;

    iSelect = 1:nBranch;
    VolumeAlive = sum(iTree(iSelect,2)-iTree(iSelect,1));

    nBranchMax = max(nBranchMax,nBranch);

    if mod(iNumIter,visual)==0
        sfParetoFront(iTree,iSelect,nCrit)
    end

    iSelect = iSelect(iTree(iSelect,1)+1<iTree(iSelect,2)) ;
    nBranchActive = length(iSelect);
    VolumeActive = sum(iTree(iSelect,2)-iTree(iSelect,1));

    if isempty(iSelect)
        Terminated = true ;
    else
        Terminated = false ;
        switch BranchSelectMethod
            case 'breadthfirst'
                iBranch = 1;
            case 'depthfirst'
                iBranch = nBranch;
            case 'minlowbound'

                % Take branch
                for iCrit=CritSeries
                    if iCrit == CritSeries(1)
                        minCrit = min(iTree(iSelect,2+iCrit));
                    else
                        minCrit = max(iTree(iSelect,2+iCrit));
                    end
                    iSelect = iSelect(iTree(iSelect,2+iCrit)==minCrit) ;
                end
                if length(iSelect)>1
                    for iCrit=CritSeries
                        minCrit = min(iTree(iSelect,2+nCrit+iCrit));
                        iSelect = iSelect(iTree(iSelect,2+nCrit+iCrit)==minCrit) ;
                    end
                end
                iBranch         =	iSelect(1)          ;
            case 'maxlowbound'

                % Take branch
                for iCrit=CritSeries
                    maxCrit = max(iTree(iSelect,2+iCrit));
                    iSelect = iSelect(iTree(iSelect,2+iCrit)==maxCrit) ;
                end
                if length(iSelect)>1
                    for iCrit=CritSeries
                        maxCrit = max(iTree(iSelect,2+nCrit+iCrit));
                        iSelect = iSelect(iTree(iSelect,2+nCrit+iCrit)==maxCrit) ;
                    end
                end
                iBranch         =	iSelect(1)          ;
            case 'minuppbound'
                % Take branch
                for iCrit=CritSeries
                    minCrit = min(iTree(iSelect,2+nCrit+iCrit));
                    iSelect = iSelect(iTree(iSelect,2+nCrit+iCrit)==minCrit) ;
                end
                if length(iSelect)>1
                    for iCrit=CritSeries
                        minCrit = min(iTree(iSelect,2+iCrit));
                        iSelect = iSelect(iTree(iSelect,2+iCrit)==minCrit) ;
                    end
                end
                iBranch         =	iSelect(1)          ;
            case 'maxuppbound'
                % Take branch
                for iCrit=CritSeries
                    maxCrit = max(iTree(iSelect,2+nCrit+iCrit));
                    iSelect = iSelect(iTree(iSelect,2+nCrit+iCrit)==maxCrit) ;
                end
                if length(iSelect)>1
                    for iCrit=CritSeries
                        maxCrit = max(iTree(iSelect,2+iCrit));
                        iSelect = iSelect(iTree(iSelect,2+iCrit)==maxCrit) ;
                    end
                end
                iBranch         =	iSelect(1)          ;
        end
        iLayoutBranch	=	iTree(iBranch,1:2)	;

        % Split into leaves
        iSplit              =	ceil(sum(iLayoutBranch)/2)  ;
        iLayoutLeaves       =	repmat(iLayoutBranch,[2 1]) ;
        iLayoutLeaves(1,2)	=	iSplit                      ;
        iLayoutLeaves(2,1)	=	iSplit                      ;
        iTree(iBranch,:)	=	nan                         ;

        % Evaluate leaves
        for iLeaf=1:2
            iLayoutBnd= iLayoutLeaves(iLeaf,:);
            [LB,UB]=fbnd(iLayoutBnd);
            iTree(nBranch+iLeaf,:) = [ iLayoutBnd LB(:)' UB(:)' ];
        end

        % Re-arrange
        xInclude                =	~isnan(iTree(:,1))	;
        nBranch                 =	sum(xInclude)       ;
        iTree(1:nBranch,:)      =	iTree(xInclude,:)	;
        iTree(nBranch+1:end,:)	=	nan                 ;

        % Fathoming
        xReference = iTree(1:nBranch,1)+1==iTree(1:nBranch,2) ;

        CritLB	=	iTree(1:nBranch,2+(1:nCrit))                        ;
        CritLBuniq	=	unique(CritLB,'rows')                        ;
        CritUBuniq	=	unique(iTree(xReference,2+(1:nCrit)+nCrit),'rows')  ;
        nRef	=	size(CritUBuniq,1)                                      ;

        sfUpdateDisplay(verbose,iNumIter,nBranch,nBranchActive,VolumeAlive,VolumeActive,Volume0,nRef,strProblem)

        xIncludeUniq = fParetoDominance(CritLBuniq,CritUBuniq);
        xInclude = ismember(CritLB,CritLBuniq(xIncludeUniq,:),'rows');

        iTree(~xInclude,:) = nan;

        % Re-arrange
        xInclude = ~isnan(iTree(:,1)) ;
        nBranch = sum(xInclude);
        iTree(1:nBranch,:) = iTree(xInclude,:);
        iTree(nBranch+1:end,:) = nan;

    end

end

iTree = iTree(1:nBranch,:) ;

iSelect = 1:nBranch;
VolumeAlive = sum(iTree(iSelect,2)-iTree(iSelect,1));

iSelect = iSelect(iTree(iSelect,1)+1<iTree(iSelect,2)) ;
nBranchActive = length(iSelect);
VolumeActive = sum(iTree(iSelect,2)-iTree(iSelect,1));

if verbose
    sfUpdateDisplay(1,iNumIter,nBranch,nBranchActive,VolumeAlive,VolumeActive,Volume0,nRef,strProblem)
end

function sfUpdateDisplay(verbose,iNumIter,nBranch,nBranchActive,VolumeAlive,VolumeActive,Volume0,nRef,strProblem)

if mod(iNumIter,verbose)==0
    clc
end
if mod(iNumIter,verbose)==0
    disp(['Problem: ' strProblem ])
    disp(['Iteration: ' num2str(iNumIter)])
    disp(['	Branches alive:    ' num2str(nBranch)])
    disp(['	Branches active:   ' num2str(nBranchActive)])
    disp(['	Branches inactive: ' num2str(nBranch-nBranchActive)])
    disp(['	Volume alive:      ' num2str(VolumeAlive/Volume0*100,'% 4.3f')])
    disp(['	Volume active:     ' num2str(VolumeActive/Volume0*100,'% 4.3f')])
    disp(['	Pareto points:     ' num2str(nRef)])
end
drawnow()

function sfParetoFront(iTree,iSelect,nCrit)

drawnow()

figure(73),
hold off
switch nCrit
    case 2
        plot(iTree(iSelect,[3 5])',iTree(iSelect,[4 6])','k-')
        hold on
        plot(iTree(iSelect,[3 ]),iTree(iSelect,[4 ]),'ko')
        plot(iTree(iSelect,[5 ]),iTree(iSelect,[6 ]),'kx')
    case 3
        plot3(iTree(iSelect,[3 6])',iTree(iSelect,[4 7])',iTree(iSelect,[5 8])','k-')
        hold on
        plot3(iTree(iSelect,[3 ]),iTree(iSelect,[4 ]),iTree(iSelect,[5 ]),'ko')
        plot3(iTree(iSelect,[6 ]),iTree(iSelect,[7 ]),iTree(iSelect,[8 ]),'kx')
        view(3)
        grid on
end
drawnow()
