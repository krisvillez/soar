function rObjectives=fEvaluate(iLayout,System,nCriterion)

% -------------------------------------------------------------------------
% SOAR toolbox - fEvaluate.m
%
% This function evaluates the objective function for a given sensor layout
% and process network.
%
% Syntax: 	rObjectives=fEvaluate(iLayout,System,nCriterion)
%
%	Inputs:
%		iLayout         Index of the sensor layout (zero-based)
%       System          Studied system
%       nCriterion      Number of criteria
%
%	Outputs:
%		rObjectives     Objective function values
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
% [2] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and
% redundancy classification in generalized process networks - II.
% Algorithms. Comp. Chem. Eng., 12(7), 689-703.
% [3] Villez, K. (2019). GENOBS* and GENRED* algorithms for observability
% and redundancy labeling.  (TR-008-01-0). Technical Report, Eawag,
% Duebendorf, Switzerland.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

global criteria
global lapse
global observableconfig

xLayout = System.lookupTable(iLayout+1,:)';
xMeasured = System.xMeasured;
xMeasured(System.linindexSensorCandidate(flipud(xLayout==1))) = 1;
System.xMeasured = xMeasured;

rObjectives = nan(nCriterion,1);

for iCriterion=1:nCriterion
    switch iCriterion
        case 1
            tBoundStart = tic;
            Cost = (sum((xMeasured(:)==1).*System.rWeightCost(:))) ;
            criteria(iLayout+1,iCriterion) = Cost ;
            tBoundElapsed = toc(tBoundStart);
            lapse(iLayout+1,iCriterion) = tBoundElapsed;
            rObjectives(iCriterion) = Cost;
        case 2
            tBoundStart = tic;
            xObservable     =	fGenObs(System,-1)             ;
            rObs = (xObservable==1).*System.rWeightObservability ;
            rFunObservable = sum(rObs(:));
            criteria(iLayout+1,iCriterion) = rFunObservable ;
            for j=1:size(System.rWeightObservability,2)
                iLayoutObs = bin2dec(num2str(xObservable(:,j)==1)')+1 ;
                observableconfig(iLayout+1,j) = iLayoutObs;
            end
            tBoundElapsed = toc(tBoundStart);
            lapse(iLayout+1,iCriterion) = tBoundElapsed;
            rObjectives(iCriterion) = rFunObservable;
        case 3
            tBoundStart = tic;
            xRedundant      =	fGenRed(System,-1)             ;
            rRed = (xRedundant==1).*System.rWeightRedundancy ;
            rFunRedundancy = sum(rRed(:)) ;
            criteria(iLayout+1,iCriterion) = rFunRedundancy ;
            tBoundElapsed = toc(tBoundStart);
            lapse(iLayout+1,iCriterion) = tBoundElapsed;
            rObjectives(iCriterion) = rFunRedundancy;
        otherwise
            disp('Unknown criterion')
    end
end

return
