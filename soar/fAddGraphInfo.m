function System =   fAddGraphInfo(System,verbose)

% -------------------------------------------------------------------------
% SOAR toolbox - fAddGraphInfo.m
%
% This function computes cycles and cutsets that are used in the GENOBS*
% and GENRED* algorithms.
%
% Syntax: 	System =   fAddGraphInfo(System,verbose)
%
%	Inputs:
%		System          System description, including graph representation
%       verbose         Level of verbosity [0-4]
%
%	Outputs:
%		System          System description with added information, such as
%                       the complete set of cycles, the complete set of
%                       cutsets, and the set of cutsets of the graph
%                       consisting of physical arcs only.
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

if verbose>=0,	disp('******* PROCESS GRAPH *******'); end

A = System.iIncidence;

if verbose>=1,  disp('	CUT SETS OF GRAPH WITHOUT ENERGY EDGES '),    end
% Cut sets of the graph composed of edges with mass effects (reactions included):
Amasseffect                         =	A                           ;
Amasseffect(:,System.xArcEnergy)	=   0                           ;
System.AcutMassEffect              =	fGetCutsets(Amasseffect)	;

% Cut sets of the graph composed of edges with mass flow (no reactions):
Amassflow                           =	A                       ;
Amassflow(:,System.xArcEnergy)	=   0                       ;
Amassflow(:,System.xArcReaction)	=   0                       ;
System.AcutMassFlow                =	fGetCutsets(Amassflow)	;

if verbose>=1,  disp('	CUT SETS OF GRAPH'),    end
% Cut sets of the complete graph:
System.Acut                         =	fGetCutsets(A)	;

if verbose>=1,  disp('	CYCLES OF GRAPH'),    end
% Cycles of the complete graph:
System.CyclesAll                    =	fGetCycles(A)     ;

CyclesPhysicalPipes             =	System.CyclesAll(:, ~any( System.CyclesAll(or(System.xArcEnergy,System.xArcReaction),:),1 ) );
System.CyclesPhysicalPipes = CyclesPhysicalPipes ;
