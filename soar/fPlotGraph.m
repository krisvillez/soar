function fPlotGraph(System)

% -------------------------------------------------------------------------
% SOAR toolbox - fPlotGraph.m
%
% This function plots the system graph.
%
% Syntax: 	fPlotGraph(System)
%
%	Inputs:
%		System          System description, including graph representation
%
%	Outputs:
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
% [2] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and
% redundancy classification in generalized process networks - II.
% Algorithms. Comp. Chem. Eng., 12(7), 689-703.
% [3] Villez, K. (2019). GENOBS* and GENRED* algorithms for observability
% and redundancy labeling.  (TR-008-01-0). Technical Report, Eawag,
% Duebendorf, Switzerland.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

% from Incidence matrix to Adjacency matrix
INCID =System.iIncidence ;
[I,J]=size(INCID);
A = zeros(I);
for j=1:J
    A(find(INCID(:,j)==-1),find(INCID(:,j)==+1)) =1;
end

Coordinates = cell2mat(System.Nodes(:,2:3)) ;

G                  =   digraph(A) ;


% plot graph with Matlab tools
p               =	plot(G)             ;
p.XData = Coordinates(:,1);
p.YData = Coordinates(:,2);
p.NodeLabel     =	System.Nodes(:,1)         ;
p.EdgeColor     =	'k'                 ;
p.NodeColor     =	'k'      ;
p.EdgeAlpha     =	1                   ;
