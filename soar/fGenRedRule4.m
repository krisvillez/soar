function xRedundant = fGenRedRule4(System,verbose,xRedundant)

% -------------------------------------------------------------------------
% SOAR toolbox - fGenRedRule4.m
%
% This function implements rule 4 of the GENRED* algorithm [1-3].
%
% Syntax: 	xRedundant = fGenRedRule4(System,verbose,xRedundant)
%
%	Inputs:
%		System          System description, including graph representation
%       verbose         Level of verbosity [0-4]
%		xRedundant      Current matrix with redundancy labels (see
%                       fGenRed.m for details)
%
%	Outputs:
%		xRedundant      Updated matrix with redundancy labels (see
%                       fGenRed.m for details)
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
% [2] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and
% redundancy classification in generalized process networks - II.
% Algorithms. Comp. Chem. Eng., 12(7), 689-703.
% [3] Villez, K. (2019). GENOBS* and GENRED* algorithms for observability
% and redundancy labeling.  (TR-008-01-0). Technical Report, Eawag,
% Duebendorf, Switzerland.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------


%% Redundancy Rule 4
% ------------------------------------------------------------------------
if verbose>=2,  disp('========================================================================================='); end
if verbose>=1,  disp('	REDUNDANCY RULE 4 '); end
if verbose>=2,  disp('========================================================================================='); end

xKnownOrMeasured = System.xKnownOrMeasured;
t = System.varindex.t ;

if verbose>=3,      xRedundant_old = xRedundant                 ;	end

% 1. Identify streams with measurable mass flow rate based on available
% mass flow rates
CyclesPhysicalPipes         	=	System.CyclesPhysicalPipes;
CyclesPhysicalPipesUnmeasured	=	CyclesPhysicalPipes(:, ~any(CyclesPhysicalPipes(xKnownOrMeasured(:,System.varindex.m)==1,:),1)  ) ;
xMassFlowObs = any([xKnownOrMeasured(:,System.varindex.m) and(~or(System.xArcEnergy,System.xArcReaction),~any(CyclesPhysicalPipesUnmeasured,2))],2) ;

% 2. Identify streams with measured temperature
xTempMeas = and(~or(System.xArcEnergy,System.xArcReaction),xKnownOrMeasured(:,System.varindex.t)) ;

% 3. Streams with observable mass flow rates and measured temperature
xMassFlowObsTempMeas = and(xMassFlowObs,xTempMeas);

% 4. Cutsets consisting only of streams identified above
Acut = System.Acut(~any(System.Acut(:,~xMassFlowObsTempMeas),2),:) ;
xRed = and(any(Acut~=0,1)',xMassFlowObsTempMeas) ;

xRedundant(xRed,t) =1;

if verbose>=3,	fDisplayLabelChanges(System,xRedundant_old,xRedundant),	end



end

