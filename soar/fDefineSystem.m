function System = fDefineSystem(iSystemID,verbose)

% -------------------------------------------------------------------------
% SOAR toolbox - fDefineSystem.m
%
% This function defines the system description, including the graph
% representation for each cases. Currently available cases:
%   'KretsovalisMah1987'
%   'KretsovalisMah1988a'
%   'KretsovalisMah1988b'
%   'VillezEtAl2016_MLE'
%   'VillezEtAl2016_MUCT'
%   'WWTP1'
%   'WWTP1reactive'
%   'WWTP1transport'
%   'WWTP2'
%   'WWTP2reactive'
%   'WWTP2transport'
%   'WWTP2transportweighted'
%
% Syntax: 	System = fDefineSystem(iSystemID,verbose)
%
%	Inputs:
%       iSystemID       System name (one of the strings above)
%       verbose         Level of verbosity [0-4]
%
%	Outputs:
%		System          System description
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

if verbose>=0,	disp('******* DEFINE SYSTEM *******'); end

switch iSystemID
    case 1
        strSystemID	=   'KretsovalisMah1987' ;
    case 2
        strSystemID	=   'KretsovalisMah1988a' ;
    case 3
        strSystemID	=   'KretsovalisMah1988b' ;
    case 4
        strSystemID	=   'VillezEtAl2016_MLE' ;
    case 5
        strSystemID	=   'VillezEtAl2016_MUCT' ;
    case 10
        strSystemID	=   'WWTP1' ;
    case 11
        strSystemID	=   'WWTP1reactive' ;
    case 12
        strSystemID	=   'WWTP1transport' ;
    case 20
        strSystemID	=   'WWTP2' ;
    case 21
        strSystemID	=   'WWTP2reactive' ;
    case 22
        strSystemID	=   'WWTP2transport' ;
    case 23
        strSystemID	=   'WWTP2transportweighted' ;
end

System.strName = strSystemID ;

% ==============================================================================================
if verbose>=1,  disp('	GRAPH STRUCTURE '),    end
% ==============================================================================================

% ---------------------------
%   Basic graph structure
% ---------------------------

switch strSystemID
    case 'KretsovalisMah1987'
        iArcEnergy              =	[ ]	;
        iArcReaction            =	[ ]	;
        iArcSplitter            =	{}  ;
        iArcEqualComposition	=	{}  ;
        iNumberOfComponent      =	3	;
        %               1   2   3   4   5   6   7
        iIncidence = [	-1	+1	0   0   0   0   0   ;   %   Stream 1
                        0   -1	0   0   0   0   +1	;   %   Stream 2
                        0   -1	+1	0   0   0   0   ;   %   Stream 3
                        0   0   +1	-1	0   0   0   ;   %   Stream 4
                        0   0   -1	+1	0   0   0   ;   %   Stream 5
                        +1	0   -1	0   0   0   0   ;   %   Stream 6
                        0   0   0   +1	-1	0   0   ;   %   Stream 7
                        -1	0   0   0   +1	0   0   ;   %   Stream 8
                        0	0   0   0   -1	+1  0   ;   %   Stream 9
                        +1	0   0   0   0   -1	0   ;   %   Stream 10
                        0   0   0   0   +1	0   -1	;   %   Stream 11
                        0   0   0   0   0   -1	+1	;   %   Stream 12
                        0   0   0   0   0   +1  -1	];	%   Stream 13

    case 'KretsovalisMah1988a'
        iArcEnergy = 11;
        iArcReaction = 12;
        iArcSplitter = {[6 7 8]};
        iArcEqualComposition = {[3 4],[6 7 8],[9 10]};
        iNumberOfComponent = 5 ;
        %               E   MX  RX  HX1	HX2 SEP SPL
        iIncidence = [	-1	+1	0	0   0   0   0	;   % 1
                        0   -1	+1	0	0   0   0   ;   % 2
                        0	0   -1	+1	0	0   0   ;   % 3
                        0   0	0   -1	0	+1	0   ;   % 4
                        +1	0   0	0   0	-1	0   ;   % 5
                        0	0   0	0   0	-1	+1  ;   % 6
                        +1	0   0	0   0	0   -1	;   % 7
                        0   +1	0	0   0	0   -1	;   % 8
                        -1   0	0	0   +1	0   0	;   % 9
                        +1   0	0	0   -1	0   0	;   % 10
                        0   0	0	-1  +1	0   0	;   % 11
                        -1   0	+1	0   0	0   0	];   % 12-14
    case 'KretsovalisMah1988b'
        iArcEnergy = [ 14 15 ];
        iArcReaction = [ 12 13 ];
        iArcSplitter = {[4 5 6]};
        iArcEqualComposition = {[3 4 5 6],[7 8]};
        iNumberOfComponent = 6 ;
        %               E	HX1	HX2	HX3	MX	RX	SPL	SEP
        iIncidence = [   -1	0	0   0   +1  0   0   0   ; % 1
                        0   0   0   0   -1  +1  0   0   ; % 2
                        0   0   0   +1  0	-1  0   0   ; % 3
                        0   0   0   -1  0	0	+1	0   ; % 4
                        +1  0   0   0	0	0	-1	0   ; % 5
                        0   0   0   0	0	0	-1	+1	; % 6
                        0   0   +1   0	0	0	0   -1	; % 7
                        0   0   -1   0	+1	0	0   0	; % 8
                        +1	0   0   0	0	0	0   -1	; % 9
                        -1	+1	0   0	0	0	0   0	; % 10
                        +1  -1	0   0	0	0	0   0	; % 11
                        -1  0   0   0   0   +1	0   0   ;   % 12 <- 12-13, 18
                        -1  0   0   0   0   +1  0   0	;   % 13 <- 14-15, 19
                        0	0   +1	-1	0   0	0	0	; % 14 <- 16
                        0	+1	0   0   0   0	0	-1	] ;   % 15 <- 17
    case 'VillezEtAl2016_MLE'
        iArcEnergy = [   ];
        iArcReaction = [   ];
        iArcSplitter = {[4 5 11],[7 9 10]};
        iArcEqualComposition = {[3 4 5 11],[7 9 10]};
        iNumberOfComponent = 3 ;

        %               g	a 	b	c 	d 	e 	f
        iIncidence = [  -1	+1	0   0   0	0	0   ;   %   1
						0   -1	+1	0   0	0	0   ;   %   2
						0   0   -1	+1	0	0	0   ;   %   3
						0   0   0	-1	+1	0	0   ;   %   4
						0   0   0	0	-1	+1	0   ;   %   5
						+1	0   0   0	0	-1	0   ;   %   6
						0   0   0	0	0   -1	+1	;   %   7
						-1	0   +1	0   0	0	0   ;   %   8
						+1	0   0   0	0	0   -1	;   %   9
						0	+1   0   0	0	0	-1  ;   %   10
						0   0   0	+1	-1	0	0   ] ;	%   11

    case 'VillezEtAl2016_MUCT'
        iArcEnergy = [   ];
        iArcReaction = [   ];
        iArcSplitter = {[ 5 6 11 ],[4 5 12],[8 9 10]};
        iArcEqualComposition = {[4 5 6 11 12],[8 9 10]};
        iNumberOfComponent = 3 ;

        %               h	a 	b	c 	d 	e 	f	g
        iIncidence = [  -1	+1	0   0   0	0	0   0	;   %   1
						0   -1	+1	0   0	0	0   0	;   %   2
						0   0   -1	+1	0	0	0   0	;   %   3
						0   0   0	-1	+1	0	0   0	;   %   4
						0   0   0	0	-1	+1	0   0	;   %   5
						0   0   0	0	0   -1	+1	0	;   %   6
						+1	0   0   0	0	0   -1	0	;   %   7
						0	0   0   0	0	0   -1	+1	;   %   8
						+1	0   0   0	0	0   0	-1	;   %   9
						0   0   +1	0	0	0   0	-1	;   %   10
						0   0   0	+1	0	-1	0   0	;   %   11
						0   +1	0   0	-1	0	0   0	] ;	%   12

    case 'WWTP1'
        iArcEnergy = [   ];
        iArcReaction = [    ];
        iArcSplitter = {[5 6 7]};
        iArcEqualComposition = {[2 3],[5 6 7]};
        iNumberOfComponent = 3 ;

        %               E	C   R   S   SPL
        iIncidence = [  -1	+1	0   0   0   ;   %   1
                        0   -1  +1  0   0   ;   %   2
                        0   0   -1  +1  0   ;   %   3
                        +1	0   0	-1	0   ;   %   4
                        0	0   0	-1	+1  ;   %   5
                        0   +1	0	0	-1  ;   %   6
                        +1	0   0	0	-1  ] ;	%	7

    case 'WWTP1reactive'
        iArcEnergy = [   ];
        iArcReaction = [ 8 9 ];
        iArcSplitter = {[5 6 7]};
        iArcEqualComposition = {[2 3],[5 6 7]};
        iNumberOfComponent = 3 ;

        %               E	C   R   S   SPL
        iIncidence = [  -1	+1	0   0   0   ;   %   1
                        0   -1  +1  0   0   ;   %   2
                        0   0   -1  +1  0   ;   %   3
                        +1	0   0	-1	0   ;   %   4
                        0	0   0	-1	+1  ;   %   5
                        0   +1	0	0	-1  ;   %   6
                        +1	0   0	0	-1  ;   %   7
                        -1  0   +1	0	0	;   %   8
                        +1  0   0	-1	0	];   %   9

    case 'WWTP1transport'
        iArcEnergy = [   ];
        iArcReaction = [  8 ];
        iArcSplitter = {[5 6 7]};
        iArcEqualComposition = {[2 3],[5 6 7]};
        iNumberOfComponent = 3 ;

        %               E	C   R   S   SPL
        iIncidence = [  -1	+1	0   0   0   ;   %   1
                        0   -1  +1  0   0   ;   %   2
                        0   0   -1  +1  0   ;   %   3
                        +1	0   0	-1	0   ;   %   4
                        0	0   0	-1	+1  ;   %   5
                        0   +1	0	0	-1  ;   %   6
                        +1	0   0	0	-1  ;	%	7
                        0   0	+1  -1	0	] ;   %   8

    case 'WWTP2'
        iArcEnergy = [   ];
        iArcReaction = [   ];
        iArcSplitter = {[5 6 12],[8 10 11]};
        iArcEqualComposition = {[3 4 5 6 12 ],[8 10 11]};
        iNumberOfComponent = 3 ;

        %               h	a 	b	c 	d 	e 	f   g
        iIncidence = [  -1	+1	0   0   0	0	0   0   ;   %   1
						0   -1	+1	0   0	0	0   0   ;   %   2
						0   0   -1	+1	0	0	0   0   ;   %   3
						0   0   0	-1	+1	0	0   0   ;   %   4
						0   0   0	0	-1	+1	0   0   ;   %   5
						0   0   0	0	0   -1	+1	0   ;   %   6
						+1	0   0   0	0	0   -1	0   ;   %   7
						0   0   0	0	0   0   -1	+1	;   %   8
						-1	0   +1	0   0	0	0   0   ;   %   9
						+1	0   0   0	0	0   0   -1	;   %   10
						0	+1   0   0	0	0	0   -1  ;   %   11
						0   0   0	+1	0   -1	0	0   ] ;	%   12
    case 'WWTP2reactive'
        iArcEnergy = [   ];
        iArcReaction = [ 13 14  ];
        iArcSplitter = {[5 6 12],[8 10 11]};
        iArcEqualComposition = {[ 5 6 12 ],[8 10 11]};
        iNumberOfComponent = 3 ;

        %               h	a 	b	c 	d 	e 	f   g
        iIncidence = [  -1	+1	0   0   0	0	0   0   ;   %   1
						0   -1	+1	0   0	0	0   0   ;   %   2
						0   0   -1	+1	0	0	0   0   ;   %   3
						0   0   0	-1	+1	0	0   0   ;   %   4
						0   0   0	0	-1	+1	0   0   ;   %   5
						0   0   0	0	0   -1	+1	0   ;   %   6
						+1	0   0   0	0	0   -1	0   ;   %   7
						0   0   0	0	0   0   -1	+1	;   %   8
						-1	0   +1	0   0	0	0   0   ;   %   9
						+1	0   0   0	0	0   0   -1	;   %   10
						0	+1   0   0	0	0	0   -1  ;   %   11
                        0   0   0	+1	0   -1	0	0   ;	%   12
                        -1	0   0   0	0	0   +1  0   ;   %   13
                        +1	0   0   0	-1  0	0   0   ] ;   %   14
	case {'WWTP2transport','WWTP2transportweighted'}
        iArcEnergy = [   ];
        iArcReaction = [ 13  ];
        iArcSplitter = {[5 6 12],[8 10 11]};
        iArcEqualComposition = {[ 5 6 12 ],[8 10 11]};
        iNumberOfComponent = 3 ;

        %               h	a 	b	c 	d 	e 	f   g
        iIncidence = [  -1	+1	0   0   0	0	0   0   ;   %   1
						0   -1	+1	0   0	0	0   0   ;   %   2
						0   0   -1	+1	0	0	0   0   ;   %   3
						0   0   0	-1	+1	0	0   0   ;   %   4
						0   0   0	0	-1	+1	0   0   ;   %   5
						0   0   0	0	0   -1	+1	0   ;   %   6
						+1	0   0   0	0	0   -1	0   ;   %   7
						0   0   0	0	0   0   -1	+1	;   %   8
						-1	0   +1	0   0	0	0   0   ;   %   9
						+1	0   0   0	0	0   0   -1	;   %   10
						0	+1   0   0	0	0	0   -1  ;   %   11
                        0   0   0	+1	0   -1	0	0   ;	%   12
                        0	0   0   0	-1  0	+1   0   ] ;   %   13


end

iIncidence=iIncidence';
[iNumberOfNode,iNumberOfArc]	=	size(iIncidence)	;

System.iNumberOfComponent       =	iNumberOfComponent	;
System.iNumberOfNode            =	iNumberOfNode       ;
System.iNumberOfArc             =	iNumberOfArc        ;

% ---------------------------
%   Arc/stream descriptions
% ---------------------------
xArcReaction                =	false(iNumberOfArc,1)   ;
xArcReaction(iArcReaction)	=	true                    ;

xArcEnergy                  =	false(iNumberOfArc,1)   ;
xArcEnergy(iArcEnergy)      =	true                    ;

xArcSplitter                =	false(iNumberOfArc,length(iArcSplitter))    ;
for s=1:length(iArcSplitter)
    xArcSplitter(iArcSplitter{s},s)	=	true            ;
end

xArcEqualComposition        =	false(iNumberOfArc,length(iArcSplitter));
for s=1:length(iArcEqualComposition)
    xArcEqualComposition(iArcEqualComposition{s},s) = true ;
end

xArcPhysical = ~or(xArcReaction,xArcEnergy);


System.iIncidence = iIncidence;
System.xArcEnergy = xArcEnergy ;
System.xArcPhysical = xArcPhysical ;
System.xArcReaction = xArcReaction ;
System.xArcSplitter = xArcSplitter ;
System.xArcEqualComposition = xArcEqualComposition ;

% ==============================================================================================
if verbose>=1,  disp('	STREAM COMPOSITION '),    end
% ==============================================================================================

ii	=	1:iNumberOfComponent        ;
t	=	iNumberOfComponent+1        ;
m	=	2*(iNumberOfComponent+1)+1	;
nn	=   (1:iNumberOfComponent)+t	;
h	=	2*t                         ;

System.varindex.ii	=   ii	;
System.varindex.t	=   t	;
System.varindex.nn	=   nn	;
System.varindex.h	=   h	;
System.varindex.m	=   m	;

Present         =	zeros(iNumberOfArc,m)	;
Present(:,m)	=	true                    ; % flow is always present
Present(:,t)	=	true                    ; % energy is always present
switch strSystemID
    case 'KretsovalisMah1987'
        Present(:,ii) = true ;
    case 'KretsovalisMah1988a'
        Present([ 1 2 3 4	6 7 8       12  ],1) = true ; % component 1
        Present([ 1 2 3 4	6 7 8       12  ],2) = true ; % component 2
        Present([ 1 2 3 4	6 7 8           ],3) = true ; % component 3
        Present([     3	4 5             12  ],4) = true ; % component 4
        Present([                 9	10      ],5) = true ; % component 5
    case 'KretsovalisMah1988b'
        Present([ 1 2 3 4 5 6 7 8 9         12	13	],1) = true ; % component 1
        Present([ 1 2 3 4 5 6 7 8 9             13	],2) = true ; % component 2
        Present([ 1 2 3 4 5 6 7 8 9         12	13	],3) = true ; % component 3
        Present([ 1 2 3 4 5 6 7 8 9         12      ],4) = true ; % component 4
        Present([   2 3 4 5 6 7 8 9         12	13	],5) = true ; % component 5
        Present([                   10	11          ],6) = true ; % component 6
    case {'WWTP1','WWTP1reactive','WWTP1transport'}
        Present(:,1) = true ; % component 1
        Present(:,2) = true ; % component 2
        Present(System.xArcPhysical,3) = true ; % component 3
    case {'VillezEtAl2016_MLE'}
        Present(:,1) = true ; % component 1
        Present(:,2) = true ; % component 2
        Present(System.xArcPhysical,3) = true ; % component 3
    case {'WWTP2','WWTP2reactive','WWTP2transport','WWTP2transportweighted'}
        Present(:,1) = true ; % component 1
        Present(:,2) = true ; % component 2
        Present(System.xArcPhysical,3) = true ; % component 3
    case {'VillezEtAl2016_MUCT'}
        Present(:,1) = true ; % component 1
        Present(:,2) = true ; % component 2
        Present(System.xArcPhysical,3) = true ; % component 3
end
Present(:,nn)	=   Present(:,ii )	;
Present(:,h)	=   Present(:,t )	;

if verbose>=1,  disp('	MEASUREMENTS '),    end

xMeasured               =	zeros(iNumberOfArc,m)	;
xMeasured(Present==0)	=   nan                     ;
xKnown                  =	zeros(iNumberOfArc,m)	;
xKnown(Present==0)      =   nan                     ;
switch strSystemID
    case 'KretsovalisMah1987'
        xMeasured([	1   	3   	5	6	7       9   				],m) = true ; % flow measurements

        xMeasured([	1   	3   		6   		9   		12  	],1) = true ; % flow measurements
        xMeasured([		2   			6   7		9   		12  	],2) = true ; % flow measurements
        xMeasured([     		4   	6   	8   9   10  	12  	],3) = true ; % flow measurements

        S =[];
    case 'KretsovalisMah1988a'

        xMeasured([     3	5 6                     ],m) = true ; % flow measurements
        xMeasured([ 1	3 4 5 6 7 8 9               ],t) = true ; % temperature measurements right after fractions

        xMeasured([ 1 2	3	  6 7 8                 ],1) = true ; % component 1
        xMeasured([       4                         ],2) = true ; % component 2
        xMeasured([       4                         ],3) = true ; % component 3
        xMeasured([     3                           ],4) = true ; % component 4

        xKnown([                            11	12	],t) = true ; % temperature measurements right after fractions

        xKnown([                                12	],1) = true ; % component 1
        xKnown([                                12	],2) = true ; % component 2
        xKnown([            5                   12  ],4) = true ; % component 4
        xKnown([                    9	10          ],5) = true ; % component 5

        S = [   -1	-1	0   +1	0   +1	]   ;

    case 'KretsovalisMah1988b'

        xMeasured([	1 2	  4       8     10  ],m) = true ; % flow measurements
        xMeasured([	1	  4	  6	7 8	9	10	],t) = true ; % temperature measurements right after fractions

        xMeasured([	1           7           ],1) = true ; % component 1
        xMeasured([	1	3	5	7	9       ],2) = true ; % component 2
        xMeasured([             7 8         ],3) = true ; % component 3
        xMeasured([	  2	  4       8	9       ],4) = true ; % component 4
        xMeasured([	  2	          8	9       ],4) = true ; % component 4
        xMeasured([      3	5	  8	9       ],5) = true ; % component 5

        xKnown([            12  13	14	15	],t) = true ; % temperature measurements right after fractions

        xKnown([            12	13          ],1) = true ; % component 1
        xKnown([                13          ],2) = true ; % component 2
        xKnown([            12	13          ],3) = true ; % component 3
        xKnown([            12              ],4) = true ; % component 4
        xKnown([            12	13          ],5) = true ; % component 5
        xKnown([	10	11                  ],6) = true ; % component 6

        S = [   -1	0	-1	+1	+1	0   +1      ;
                -1	+1	+1	0	-1	0   +1	]   ;
    case 'VillezEtAl2016_MLE'
        xMeasured([ 1 3 8 9 10 11                 ],m) = true ; % flow measurements
        xMeasured([             ],t) = true ; % temperature measurements right after fractions

        xMeasured([ 1 4 8 10               ],1) = true ; % component 1

        switch strSystemID
            case 'WWTP2'
                S =[];
            case 'WWTP2reactive'
                S =[ +1 -1 0 +1 ; +1 -1 0 +1 ];
            case 'WWTP2transport'
                S =[ +1 -1 0 +1 ];
        end
    case 'VillezEtAl2016_MUCT'
        xMeasured([ 1 3 4 9 10 11  12               ],m) = true ; % flow measurements
        xMeasured([             ],t) = true ; % temperature measurements right after fractions

        xMeasured([ 1 5 7 8 12               ],1) = true ; % component 1

        S =[];
    case {'WWTP1','WWTP1reactive','WWTP1transport'}
%         xMeasured([ 1 6 4                 ],m) = true ; % flow measurements
%         xMeasured([             ],t) = true ; % temperature measurements right after fractions
%
%         xMeasured([ 1 3 5              ],1) = true ; % component 1
        switch strSystemID
            case 'WWTP1'
                S =[];
            case 'WWTP1reactive'
                S =[ +1 -1 0 +1 ; +1 -1 0 +1 ];
            case 'WWTP1transport'
                S =[ +1 -1 0 +1 ];
        end
    case {'WWTP2','WWTP2reactive','WWTP2transport','WWTP2transportweighted'}
        xKnown(9,1:3)=true ; % fraction components in carbon dosage assumed known

        switch strSystemID
            case 'WWTP2'
                S =[];
            case 'WWTP2reactive'
                S =[ +1 -1 0 +1 ; +1 -1 0 +1 ];
            case 'WWTP2transport'
                S =[ +1 -1 0 +1 ];
            case 'WWTP2transportweighted'
                S =[ +1 -1 0 +1 ];
                xKnown(9:12,m)=true ; % controlled flow rates
        end
end

xArcPure            =	sum(~isnan(xMeasured(:,System.varindex.ii)),2)<=1 ; % only one component or pure energy flow

System.xKnown       =	xKnown      ;
System.xMeasured	=	xMeasured	;
System.xArcPure     =	xArcPure	;

% ========================================================================
%   GROUND TRUTH
% ========================================================================

if verbose>=1,  disp('	GROUND TRUTH '),    end
switch strSystemID
    case 'KretsovalisMah1988a'
        xRefObservable = [];
        xRefRedundancy = [];

    case 'KretsovalisMah1988b'
        xRefObservable = xMeasured;

        xRefObservable([  1	2 3	  5 6 7	8 9 10  11  12  13	14  15	],m)        =	true	;

        xRefObservable([  1	2 3 4 5 6	  9	10  11  12  13  14	15	],t)        =	true    ;

        xRefObservable([  1	2 3 4 5	6 7	8 9         12	13          ],ii(1))    =	true    ;
        xRefObservable([  1	2 3	4 5	6 7	8 9             13          ],ii(2))    =	true    ;
        xRefObservable([  1	2 3	4 5	6 7	8 9         12	13          ],ii(3))    =	true    ;
        xRefObservable([  1	2 3	4 5	6 7	8 9         12              ],ii(4))    =	true    ;
        xRefObservable([	2 3 4 5	6 7	8 9         12	13          ],ii(5))    =	true    ;
        xRefObservable([                    10	11                  ],ii(6))    =	true    ;

        xRefObservable([  1	2 3	4 5	6 7	8 9	10	11	12	13	14	15	],h)        =	true    ;

        xRefObservable([  1	2 3 4 5	6 7	8 9         12	13          ],nn(1))    =	true    ;
        xRefObservable([  1	2 3 4 5	6 7	8 9         	13          ],nn(2))    =	true    ;
        xRefObservable([  1	2 3 4 5	6 7	8 9         12	13          ],nn(3))    =	true    ;
        xRefObservable([  1	2 3 4 5	6 7	8 9         12              ],nn(4))    =	true    ;
        xRefObservable([	2 3 4 5	6 7	8 9         12	13          ],nn(5))    =	true    ;
        xRefObservable([                    10  11                  ],nn(6))    =	true    ;

        xRefRedundancy	=	nan(size(xMeasured))        ;

        I	=	[	7     1     3     5     7     9     7     8     2     4     8     9     3     5     8     9     1     4     6     7     1     2     4     8 ];
        J	=	[	1     2     2     2     2     2     3     3     4     4     4     4     5     5     5     5     7     7     7     7    15    15    15    15 ];
        IND	=       sub2ind(size(xRefRedundancy),I,J)   ;
        xRefRedundancy(IND)	=   +1 ;

        I	=	[   1     8     9    10    10	]       ;
        J	=	[   1     7     7     7    15	]       ;
        IND	=       sub2ind(size(xRefRedundancy),I,J)   ;
        xRefRedundancy(IND)	=	-1 ;
    otherwise
        % do nothing
        xRefObservable = [];
        xRefRedundancy = [];
end

System.reference.xObservable	=	xRefObservable      ;
System.reference.xRedundant     =	xRefRedundancy      ;


% ========================================================================
%   SPECIFY POSSIBLE SENSOR LOCATIONS AND WEIGHTS FOR OBSERVABILITY
% ========================================================================

switch strSystemID
    case {'KretsovalisMah1987', 'KretsovalisMah1988a', 'KretsovalisMah1988b'}
        xSensorCandidate = Present ;
        xSensorCandidate(:,nn) = false ;
    otherwise
        xSensorCandidate = Present ;
        xSensorCandidate(:,:) = false ;
        xSensorCandidate(:,m) = all([ Present(:,m) xKnown(:,m)~=1 xArcPhysical ],2);
        xSensorCandidate(:,1) = all([ Present(:,1) xKnown(:,1)~=1 xArcPhysical ],2);
end
switch strSystemID
    case {'KretsovalisMah1987', 'KretsovalisMah1988a', 'KretsovalisMah1988b'}
        rWeightObservability = Present ;
        rWeightObservability(:,:) =1 ;
    case 'WWTP2transportweighted'
        rWeightObservability = Present ;
        rWeightObservability(:,:) = 0 ;
        rWeightObservability([1 6 7 9:12],m) = 1;
        rWeightObservability([1 6 7 9:10 ],ii(1)) = 1;
        rWeightObservability([1 4 7:9 13],nn(1)) = 1;
    otherwise
        rWeightObservability = Present ;
        rWeightObservability(:,:) = 0 ;
        rWeightObservability(:,m) = 1;
        rWeightObservability(:,ii(1)) = 1;
        rWeightObservability(:,nn(1)) = 1;
end

rWeightCost=(xSensorCandidate==1)*1;
rWeightCost(:,:)=1;
rWeightRedundancy=xSensorCandidate*1;
System.rWeightCost = rWeightCost ;
System.rWeightObservability = rWeightObservability ;
System.rWeightRedundancy = rWeightRedundancy ;
System.xSensorCandidate = xSensorCandidate ;



% ========================================================================
%   SYMBOLIC VARIABLES
% ========================================================================

if exist('syms')

    if verbose>=1,  disp('	SYMBOLIC VARIABLES '),    end

    syms sym_M sym_N sym_H sym_x sym_t sym_values
    sym_Rate                =	sym('M%d', [iNumberOfArc 1])                        ;
    sym_HeatFlow            =	sym('H%d', [iNumberOfArc 1])                        ;
    sym_temperature         =	sym('t%d', [iNumberOfArc 1])                        ;
    sym_ComponentFlow       =	sym('N%d_%d', [iNumberOfArc iNumberOfComponent])    ;
    sym_ComponentFraction	=	sym('x%d_%d', [iNumberOfArc iNumberOfComponent])	;

    % Account for splitter arcs:
    for s=1:size(xArcSplitter,2)
        xSelect                     =	xArcSplitter(:,s)                           ;
        sym_row                     =	sym_temperature(find(xSelect,1,'first'),:)  ;
        sym_temperature(xSelect,:)	=	repmat(sym_row,[sum(xSelect) 1])            ;
    end

    % Account for equal composition:
    for s=1:size(xArcEqualComposition,2)
        xSelect                             =	xArcEqualComposition(:,s)                           ;
        sym_row                             =	sym_ComponentFraction(find(xSelect,1,'first'),:)	;
        sym_ComponentFraction(xSelect,:)	=	repmat(sym_row,[sum(xSelect) 1])                    ;
    end

    % Account for known stoichiometry, assuming known energy contribution:
    if any(xArcReaction)
        sym_ComponentFraction(xArcReaction,[System.varindex.ii(:) ; System.varindex.t])	=	S    ;
    end

    % Account for known 100% energy density:
    sym_ComponentFraction(xArcEnergy,System.varindex.ii)	=	0               ;
    sym_temperature(xArcEnergy,1)                           =	1               ;

    sym_MassFlow                =	sym_Rate	;
    sym_MassFlow(xArcReaction)	=	0           ;
    sym_MassFlow(xArcEnergy)	=	0           ;
    sym_ComponentFraction(isnan(xMeasured(:,System.varindex.ii)))   =   0	;
    sym_ComponentFlow(isnan(xMeasured(:,System.varindex.ii)))       =   0   ;

    global OneComponent
    if ~OneComponent
        System.symbolic.sym_ProcessRate         =	sym_Rate                ;
        System.symbolic.sym_MassFlow            =	sym_MassFlow            ;
        System.symbolic.sym_HeatFlow            =	sym_HeatFlow            ;
        System.symbolic.sym_Temperature         =	sym_temperature         ;
        System.symbolic.sym_ComponentFlow       =	sym_ComponentFlow       ;
        System.symbolic.sym_ComponentFraction	=	sym_ComponentFraction	;
    end
end

end
