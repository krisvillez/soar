function xRedundant = fGenRedRule5(System,verbose,xRedundant)

% -------------------------------------------------------------------------
% SOAR toolbox - fGenRedRule5.m
%
% This function implements rule 5 of the GENRED* algorithm [1-3].
%
% Syntax: 	xRedundant = fGenRedRule5(System,verbose,xRedundant)
%
%	Inputs:
%		System          System description, including graph representation
%       verbose         Level of verbosity [0-4]
%		xRedundant      Current matrix with redundancy labels (see
%                       fGenRed.m for details)
%
%	Outputs:
%		xRedundant      Updated matrix with redundancy labels (see
%                       fGenRed.m for details)
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
% [2] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and
% redundancy classification in generalized process networks - II.
% Algorithms. Comp. Chem. Eng., 12(7), 689-703.
% [3] Villez, K. (2019). GENOBS* and GENRED* algorithms for observability
% and redundancy labeling.  (TR-008-01-0). Technical Report, Eawag,
% Duebendorf, Switzerland.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------


%% Redundancy Rule 5
% ------------------------------------------------------------------------
if verbose>=2,  disp('========================================================================================='); end
if verbose>=1,  disp('	REDUNDANCY RULE 5 '); end
if verbose>=2,  disp('========================================================================================='); end

if verbose>=3,      xRedundant_old = xRedundant                 ;	end

xKnownOrMeasured = System.xKnownOrMeasured;
ii = System.varindex.ii ;
C = length(ii);

% 5a -> meaasurements x redundant if corresponding edge is on a cutset of
% graph consisting of edges with observable mass flows and measured
% fractions

% 1. Identify streams with measurable mass flow rate based on available
% mass flow rates
CyclesPhysicalPipes = System.CyclesPhysicalPipes  ;
CyclesPhysicalPipesUnmeasured	=	CyclesPhysicalPipes(:, ~any(CyclesPhysicalPipes(xKnownOrMeasured(:,System.varindex.m)==1,:),1)  ) ;
xMassFlowObs = any([xKnownOrMeasured(:,System.varindex.m) and(~or(System.xArcEnergy,System.xArcReaction),~any(CyclesPhysicalPipesUnmeasured,2))],2) ;

iNumberOfArc= System.iNumberOfArc;
iNumberOfComponent= System.iNumberOfComponent;
xComponentFlowObs = true(iNumberOfArc,iNumberOfComponent) ;
for iComponent=1:iNumberOfComponent

    col= System.varindex.ii(iComponent);

    % 2. Identify streams with measured fraction
    xFracMeas = and(~or(System.xArcEnergy,System.xArcReaction),or(isnan(xKnownOrMeasured(:,col)),xKnownOrMeasured(:,col)==1) ) ;

    % 3. Streams with observable mass flow rates, measured fraction,
    % excluding pure component arcs
    xMassFlowObsFracMeas = all( [	xMassFlowObs	xFracMeas	~System.xArcPure	],2)    ;

    % 4. Cutsets consisting only of streams identified above
    AcutMassEffect  = System.AcutMassEffect (~any(System.AcutMassEffect(:,~xMassFlowObsFracMeas),2),:) ;
    xRed = all( [	any(AcutMassEffect~=0,1)'	xMassFlowObsFracMeas	xKnownOrMeasured(:,col)==1  ],2) ;

    xRedundant(xRed,col) =1;

    % 5b(i) Prep for next step -> determine which component flows are observable
    % based on mass flow measurements and measurements of the considered
    % component fractions

    % Remove cycles with pure energy arcs and with observable mass flows:
    CyclesNoComponentFlowObs = System.CyclesAll(:,~any(System.CyclesAll(or(xMassFlowObsFracMeas,System.xArcEnergy),:),1));
    % Streams on cycles have non-observable flow
    xComponentNotObs = any(CyclesNoComponentFlowObs,2);
    xComponentFlowObs(xComponentNotObs,iComponent) = 0;
    xComponentFlowObs(System.xArcPure,iComponent)  =0;
    xComponentFlowObs(System.xArcReaction,iComponent)  =0;

end

if verbose>=3,	fDisplayLabelChanges(System,xRedundant_old,xRedundant),	end

% 5b(ii) -> a fraction measurement x_k,h for component k on stream h is
% redundant if it holds for each component i (1:I) that the component flow
% Ni,h in the stream h is observable through mass flow measurements (M_j,
% j=1:J) and fraction measurement of that component (x_i,j, j=1:J).
xAllComponentFlowsObservable = all(xComponentFlowObs,2) ;

xRedundant(and(repmat(xAllComponentFlowsObservable,[1 C]),xKnownOrMeasured(:,ii)==1)) = 1;
if verbose>=3,	fDisplayLabelChanges(System,xRedundant_old,xRedundant),	end



end

