function [xModalityIncluded,estimatedflows,C_,b_] = fProcedureSystem(System,xObservability,a_,verbose)

% -------------------------------------------------------------------------
% SOAR toolbox - fProcedureSystem.m
%
% This function implements the procedure SYSTEM in rule 6 of the GENOBS*
% algorithm [1-3]. 
%
% Syntax: 	[xModalityIncluded,estimatedflows,C_,b_] 
%               = fProcedureSystem(System,xObservability,a_,verbose) 
%
%	Inputs: 	
%		System          System description, including graph representation
%       xObservability	Current matrix with observability labels (see
%                       fGenObs.m for details) 
%       a_              Coefficients of the considered balance equation
%                       (+1, -1, 0)
%       verbose         Level of verbosity [0-4]
%
%	Outputs: 	
%		xModalityIncluded	Boolean indicator indicating which balance
%                           equations are included
%       estimatedflows      Boolean indicator indicating which flow rates
%                           may be observable
%       C_                  Constraint matrix (left-hand side)
%       b_                  Contraint vector (right-hand side)
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2019-07-24
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2019 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


cutset	=   find(a_)'       ;
a_      =	a_(:,cutset)	;

if verbose>=4,	disp(['         Cutset: ' num2str(cutset')]),   end


iNumberOfComponent	=   System.iNumberOfComponent	;
m                   =	System.varindex.m           ;

xArcEnergy          =   System.xArcEnergy           ;
xArcPhysical        =   System.xArcPhysical         ;
xArcReaction        =   System.xArcReaction         ;
xArcSplitter        =   System.xArcSplitter         ;

% ========================================================================
%   PROCEDURE SYSTEM
% ========================================================================

% Preliminary + Rule A.(a-c)
% Set 1
xFlowSet1                   =	false(size(cutset))                                 ;
xMassFlowObservable         =	xObservability(cutset,m)                            ;
xMassFlowLabeled            =	and(xArcPhysical(cutset),xMassFlowObservable~=0)	;
xFlowSet1(xMassFlowLabeled)	=	true                                                ;

for s=1:size(xArcSplitter,2)
    if sum(xArcSplitter(cutset,s))>1
        xFlowSet1(xArcSplitter(cutset,s))	=	true	;
    end
end
xFlowSet1(xArcEnergy(cutset))	=	true                ;

% Set 2
xFlowSet2                   =	false(size(cutset))                                 ;
xMassFlowObservable         =	xObservability(cutset,m)                            ;
xMassFlowLabeled            =	and(xArcReaction(cutset),xMassFlowObservable~=0)	;
xFlowSet2(xMassFlowLabeled) =	true                                                ;
xFlowSet2(xFlowSet1)        =   false                                               ;

% Set 3
xFlowSet3                   =	~or(xFlowSet1,xFlowSet2)    ;

% Initialization
C_ = sparse(0,sum(xFlowSet3));
b_ = sparse(0,0);
xModalityIncluded           =	false(iNumberOfComponent+2,1)  ;

if ~any(xFlowSet3)
    if verbose>=4,	disp( '         No unknown mass flows available for estimation -> Skip this cutset'),	end
    estimatedflows = [];
else
    if verbose>=4,	disp(['         Unknown flows: ' num2str(cutset(xFlowSet3)')]),	end
    
    if verbose>=4,	disp( '         Execute procedure SYSTEM' ), end
    
    ii      =	System.varindex.ii      ;
    h       =	System.varindex.h       ;
    t       =	System.varindex.t       ;
    
    %sym_R	=	System.symbolic.sym_ProcessRate	;
    sym_M	=	System.symbolic.sym_MassFlow	;
    sym_H	=	System.symbolic.sym_HeatFlow	;
    sym_N	=	System.symbolic.sym_ComponentFlow	;
    sym_t	=	System.symbolic.sym_Temperature	;
    sym_x	=	System.symbolic.sym_ComponentFraction	;
    
    c0_      =	+transpose(a_(xFlowSet3)).*sym_x(cutset(xFlowSet3),:);
    if any(xFlowSet1)
        b0	=	-transpose(a_(xFlowSet1)).*sym_N(cutset(xFlowSet1),:);
    else
        b0  = sparse(1,iNumberOfComponent);
    end
    
    % component balances:
    
    for iComponent = ii(:)'
        
        n	=	iNumberOfComponent+iComponent+1	;
        x	=	iComponent                          ;
        
        xComponentFractionObservable	=	xObservability(cutset,x)                ;
        xComponentFlowObservable        =	xObservability(cutset,n)                ;
        
        xComponentFractionObservable(isnan(xComponentFractionObservable))	=   1   ;
        xComponentFlowObservable(isnan(xComponentFlowObservable))           =	1   ;
        
        if all(xComponentFlowObservable(xFlowSet1)==1) % Rule A.(c)
            iFlowSet3 = find(xFlowSet3);
            %             c_      =	+a_(xFlowSet3).*transpose(sym_x(cutset(xFlowSet3),iComponent));
            %             b       =	-(a_(xFlowSet1)) *sym_N(cutset(xFlowSet1),iComponent);
            replace =	find(and(xComponentFractionObservable<=0,xFlowSet3)) ;
            %             disp('try')
            %             c_
            %             b
            c_ = transpose(c0_(:,iComponent));
            b = sum(b0(:,iComponent),1) ;
            %             c_
            %             b
            for f=replace(:)'
                if xComponentFlowObservable(f) % Rule A.(d)(ii)
                    c_(f==iFlowSet3)	=   0   ;
                    b       =	b+sym_N(cutset(f),iComponent);% Rule A.(d)(i)
                else
                    c_(f==iFlowSet3)	=   nan	;
                end
            end
            if all(~isnan(c_)) % Rule A.(d)(ii)
                C_	=	[	C_	;	c_	]   ;
                b_	=	[	b_	;	b	]   ;
                xModalityIncluded(iComponent)=true;
            end
        end
    end
    
    % energy balance
    xEnergyFlowObservable	=	xObservability(cutset,h)    ;
    xTemperatureObservable	=	xObservability(cutset,t)    ;
    
    if all(xEnergyFlowObservable(xFlowSet1)==1) % Rule A.(b)
        % Rule A.(d)
        c_      =	a_(xFlowSet3).*transpose(sym_t(cutset(xFlowSet3)))  ;
        b       =	-(a_(xFlowSet1)) *sym_H(cutset(xFlowSet1))          ;
        replace =	find(and(xTemperatureObservable<=0,xFlowSet3))      ;
        
        for f=replace(:)'
            if xEnergyFlowObservable(f) % Rule A.(d)(ii)
                c_(f)	=   0   ;
                b       =	b+sym_H(cutset(f),iComponent);% Rule A.(d)(i)
            else
                c_(f)	=   nan	;
            end
        end
        if all(~isnan(c_)) % Rule A.(d)(ii)
            C_	=	[	C_	;	c_	]   ;
            b_	=	[	b_	;	b	]   ;
            xModalityIncluded(end-1)	=   true	;
        end
    end
    
    % mass flow balance
    if all(or(xMassFlowObservable(xFlowSet1)==1,xArcEnergy(cutset(xFlowSet1)))) % Rule A.(a)
        c_	=	+a_(xFlowSet3)                              ;
        b	=	-a_(xFlowSet1)*sym_M(cutset(xFlowSet1))    ;
        C_	=	[	C_	;	c_	]                           ;
        b_	=	[	b_	;	b	]                           ;
        xModalityIncluded(end)	=   true                    ;
    end
    
    % Rule B.
    if any(xFlowSet2)
        inc = cutset(xFlowSet2) ;
        coeff       =	transpose([ sym_x(inc,xModalityIncluded(1:end-2)) sym_t(inc,xModalityIncluded(end-1)) ]);
        if xModalityIncluded(end)
            coeff	=	[	coeff	;	zeros(1,size(coeff,2))	]   ;
        end
        C_          =	[   C_	coeff	]   ;
    end
    
    % Rule C.
    iModalityIncluded	=	find(xModalityIncluded)	;
    
    NonZero	=   ~all(C_==0,2)	;
    C_      =	C_(NonZero,:)	;
    b_      =	b_(NonZero)     ;
    
    xModalityIncluded(iModalityIncluded(~NonZero)) =0;
    
    estimatedflows	=	cutset(or(xFlowSet2,xFlowSet3))           ;
    if isempty(C_)
        estimatedflows =[];
    end
    if verbose>=4  && ~isempty(C_)
        u_              =	sym('u',[size(C_,2), 1])    ;
        eqs             =	C_*u_ ==b_                  ;
        fprintf('----------------------------------------\n\n')
        disp(eqs)
        fprintf('----------------------------------------\n')
    end
    
        if verbose>=4, disp(['           Procedure SYSTEM done' ]), end
end

end
