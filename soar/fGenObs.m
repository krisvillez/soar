function xObservable =  fGenObs(System,verbose)

% -------------------------------------------------------------------------
% SOAR toolbox - fGenObs.m
%
% This function implements the GENOBS* algorithm used in [1], based on the
% GENOBS algorithm in [2]. A description of GENOBS* can be found in [3].
%
% Syntax: 	xObservable = fGenObs(System,verbose)
%
%	Inputs:
%		System          System description, including graph representation
%       verbose         Level of verbosity [0-4]
%
%	Outputs:
%		xObservable     Matrix consisting of integers indicating the
%                       observability labels (1: observable, -1:
%                       unobservable, 0: no label, nan: variable does not
%                       exist; dimensions: number or arcs X number of
%                       variables)
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
% [2] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and
% redundancy classification in generalized process networks - II.
% Algorithms. Comp. Chem. Eng., 12(7), 689-703.
% [3] Villez, K. (2019). GENOBS* and GENRED* algorithms for observability
% and redundancy labeling.  (TR-008-01-0). Technical Report, Eawag,
% Duebendorf, Switzerland.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

%%
% **************************************************************************************
% Rule 0 - Setup
% **************************************************************************************

xObservable =  System.xKnown ; % Label mass fraction
xObservable(System.xMeasured==1) = 1 ;

if verbose>=0,	disp('******* OBSERVABILITY *******'); end

xObsNotMassFlow = xObservable(:,1:end-1) ;

if ~any(xObsNotMassFlow(:))
    xObservable = fAssignMassFlow(System,xObservable)	;
    xObservable(xObservable==0) = -1;
else
    xAllLabeled             =	false	;
    xGenobsIterations       =	0       ;
    xGenobsIterationsMax	=   inf     ;

    while (~xAllLabeled) && (xGenobsIterations<=xGenobsIterationsMax)

        xGenobsIterations = xGenobsIterations+1;

        %%
        % ************
        %	Rule 1+2
        % ************
        xObservable = fGenObsRule1(System,verbose,xObservable) ;
        xObservable = fGenObsRule2(System,verbose,xObservable) ;

        %%
        % ************
        %	Rule 3+4
        % ************
        iRule34_iterations = 0 ;
        xRule34_converged = false;
        while ~xRule34_converged
            xObservability_before34 =xObservable;
            iRule34_iterations = iRule34_iterations +1 ;

            xObservable = fGenObsRule3(System,verbose,xObservable) ;
            xObservable = fGenObsRule4(System,verbose,xObservable) ;

            % Rule 4e
            if verbose>=2,	disp('	Rule 4e         Check convergence'); end
            if ~any(and(xObservability_before34(:)~=xObservable(:),~isnan(xObservable(:))))
                xRule34_converged = true ;
            end
        end

        %%
        % ************
        %	Rule 5+6
        % ************
        xObservable = fGenObsRule5and6(System,verbose,xObservable) ;
        xAllLabeled = ~any(xObservable(:)==0);

    end
end

if verbose>=2,	disp('	All variables are labelled. Done!'), end
if verbose>=1,  disp('	OBSERVABILITY DONE '),    end
