function [LB,UB,xLayoutL,xLayoutU]=fBound(iLayoutBnd,System,nCriterion)

% -------------------------------------------------------------------------
% SOAR toolbox - fBound.m
%
% This function computes bounds to the objective functions given the set of
% sensor layouts that can be build with the sensors available in the union
% of two sensor layouts. See [1] for details.
%
% Syntax: 	[LB,UB]=fBound(iLayoutBnd,System,nCriterion)
%
%	Inputs:
%		iLayoutBnd	Two integers specifying the indices of two sensor
%                   layouts
%       nCriterion  Number of objective functions
%
%	Outputs:
%		LB          Lower bound values for the objective functions
%		UB          Upper bound values for the objective functions
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

global lapse
global criteria
global RedundancyDeficit
global observableconfig

iLayoutL = iLayoutBnd(1);
iLayoutU = iLayoutBnd(2);

% nSensorCandidate = System.nSensorCandidate ;
% xLayoutL = str2num(dec2bin(iLayoutL,nSensorCandidate)');
% xLayoutU = str2num(dec2bin(iLayoutU-1,nSensorCandidate)');

xLayoutL = System.lookupTable(iLayoutL+1,:)';
xLayoutU = System.lookupTable(iLayoutU,:)';

xLayoutMin = xLayoutL;
xLayoutMin(xLayoutL~=xLayoutU) = 0 ;
xMeasuredMin = System.xMeasured;
xMeasuredMin(System.linindexSensorCandidate(flipud(xLayoutMin==1))) = 1;

xLayoutMax = xLayoutU;
xLayoutMax(xLayoutL~=xLayoutU) = 1 ;
iLayoutMax = sum(xLayoutMax(:).*(2.^((length(xLayoutMax)-1):-1:0))')+1;
%iLayoutMax = bin2dec(num2str(xLayoutMax(:)'))+1;

xMeasuredMax = System.xMeasured;
xMeasuredMax(System.linindexSensorCandidate(flipud(xLayoutMax==1))) = 1;
System.xMeasured = xMeasuredMax;

LB = nan(nCriterion,1);
UB = nan(nCriterion,1);

for iCriterion=1:nCriterion
    switch iCriterion
        case 1
            tBoundStart = tic;
            MaxCost = floor(sum((xMeasuredMax(:)==1).*System.rWeightCost(:))) ;
            MinCost = ceil(sum((xMeasuredMin(:)==1).*System.rWeightCost(:))) ;
            LB(iCriterion) = MinCost;
            UB(iCriterion) = MaxCost;
            criteria(iLayoutMax,iCriterion) = MaxCost ;
            tBoundElapsed = toc(tBoundStart);
            lapse(iLayoutMax,iCriterion) = tBoundElapsed;
        case 2
            if isnan(criteria(iLayoutMax,iCriterion))
                tBoundStart = tic;
                xObservable     =	fGenObs(System,-1)             ;
                %xObservable     =	fGenObs(System,+3)             ;
                rObs = (xObservable==1).*System.rWeightObservability ;
                rFunObservable = sum(rObs(:));
                criteria(iLayoutMax,iCriterion) = rFunObservable ;
                for j=1:size(System.rWeightObservability,2)
                    iLayoutObs = bin2dec(num2str(xObservable(:,j)==1)')+1 ;
                    %str2num(dec2bin(iLayoutObs-1,size(System.rWeightObservability,1))')
                    observableconfig(iLayoutMax,j) = iLayoutObs;
                end
                tBoundElapsed = toc(tBoundStart);
                lapse(iLayoutMax,iCriterion) = tBoundElapsed;
            else
                rFunObservable = criteria(iLayoutMax,iCriterion);
            end
            LB(iCriterion) = -floor(rFunObservable);
            UB(iCriterion) = -ceil(rFunObservable);
        case 3

            if isnan(criteria(iLayoutMax,iCriterion))
                tBoundStart = tic;
                xRedundant      =	fGenRed(System,-1)             ;
                %xRedundant      =	fGenRed(System,+3)             ;
                rRed = (xRedundant==1).*System.rWeightRedundancy ;
                rFunRedundancy = sum(rRed(:)) ;
                criteria(iLayoutMax,iCriterion) = rFunRedundancy ;
                tBoundElapsed = toc(tBoundStart);
                lapse(iLayoutMax,iCriterion) = tBoundElapsed;
            else
                rFunRedundancy = criteria(iLayoutMax,iCriterion);
            end
            if RedundancyDeficit
                rFunRedundancyLo = rFunRedundancy-MaxCost;
                rFunRedundancyHi = rFunRedundancy-MinCost;
            else
                rFunRedundancyLo = rFunRedundancy;
                rFunRedundancyHi = rFunRedundancy;
            end
            LB(iCriterion) = floor(-rFunRedundancyHi);
            UB(iCriterion) = ceil(-rFunRedundancyLo);
        otherwise
            disp('Unknown criterion')
    end
end
