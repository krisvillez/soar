function xRedundant = fGenRedRule7(System,verbose,xRedundant)

% -------------------------------------------------------------------------
% SOAR toolbox - fGenRedRule7.m
%
% This function implements rule 7 of the GENRED* algorithm [1-3].
%
% Syntax: 	xRedundant = fGenRedRule7(System,verbose,xRedundant)
%
%	Inputs:
%		System          System description, including graph representation
%       verbose         Level of verbosity [0-4]
%		xRedundant      Current matrix with redundancy labels (see
%                       fGenRed.m for details)
%
%	Outputs:
%		xRedundant      Updated matrix with redundancy labels (see
%                       fGenRed.m for details)
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
% [2] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and
% redundancy classification in generalized process networks - II.
% Algorithms. Comp. Chem. Eng., 12(7), 689-703.
% [3] Villez, K. (2019). GENOBS* and GENRED* algorithms for observability
% and redundancy labeling.  (TR-008-01-0). Technical Report, Eawag,
% Duebendorf, Switzerland.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------


%% Redundancy Rule 7
% ------------------------------------------------------------------------
if verbose>=2,  disp('========================================================================================='); end
if verbose>=1,  disp('	REDUNDANCY RULE 7 '); end
if verbose>=2,  disp('========================================================================================='); end

xMeasured= System.xMeasured;
xKnownOrMeasured = System.xKnownOrMeasured;
%	--------------
%        7(a)
%	--------------

if verbose>=3,      xRedundant_old = xRedundant                 ;	end

ii = System.varindex.ii ;
t = System.varindex.t ;

xCandidate = all([System.xArcPhysical xMeasured(:,t)==1 xRedundant(:,t)==0],2);

% find cycles without arcs that are reaction edges or splitter arcs (cycles in Grs)
Cycles0 = System.CyclesAll(:,~any(System.CyclesAll(any([System.xArcReaction System.xArcSplitter],2),:),1));

% find subset of cycles which have one temperature measurement among the
% candidate measurements and no other temperature measurement
Cycles1 = Cycles0(:,and(...
    sum(Cycles0(all([System.xArcPhysical xKnownOrMeasured(:,t)==1 xRedundant(:,t)==0],2),:),1)==1, ...
    sum(Cycles0(all([System.xArcPhysical xKnownOrMeasured(:,t)==1 xRedundant(:,t)~=0],2),:),1)==0 ));

iCandidate = find(xCandidate);
for iStream=iCandidate(:)'
    % Cycles with temperature measurement in considere stream
    Cycles_i_t = Cycles1(:,Cycles1(iStream,:)==1);
    if size(Cycles_i_t,2)>=1
        % this means the arcs form a cycle -> label measurement nonredundant
        xRedundant(iStream,t) = -1;
    end
end

%	--------------
%        7(b)
%	--------------

nCycle = size(System.CyclesAll,2);
xInclude = true(nCycle,1) ;
for iComponent = ii(:)'
    nCycle = size(System.CyclesAll,2);
    xSomeFracMeas = any(and(repmat(xMeasured(:,iComponent)==1,[1 nCycle]),System.CyclesAll),1);
    xInclude(xSomeFracMeas) = false;
end

Cycles = System.CyclesAll(:,xInclude) ;

for iComponent = ii(:)'
    xStreamCandidate = all([xMeasured(:,iComponent)==1 xRedundant(:,iComponent)==0],2);

    % cycles with one of the candidate measurements for fraction i and only
    % one measurement for fraction i
    Cycles_i = Cycles(:,and(sum(Cycles(xStreamCandidate,:),1)==1,sum(Cycles(xMeasured(:,iComponent)==1,:),1)==1));
    nCycle = size(Cycles_i,2);
    xNonRed =any(and(repmat(xStreamCandidate,[1 nCycle]),Cycles_i),2) ;
    if any(xNonRed)
        xRedundant(xNonRed,iComponent) = -1;
        disp('This rule has not been inspected upon activation - please check results carefully ')
    end

end

if verbose>=3,	fDisplayLabelChanges(System,xRedundant_old,xRedundant),	end



end

