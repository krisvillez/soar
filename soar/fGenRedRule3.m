function xRedundant = fGenRedRule3(System,verbose,xRedundant)

% -------------------------------------------------------------------------
% SOAR toolbox - fGenRedRule3.m
%
% This function implements rule 3 of the GENRED* algorithm [1-3].
%
% Syntax: 	xRedundant = fGenRedRule3(System,verbose,xRedundant)
%
%	Inputs:
%		System          System description, including graph representation
%       verbose         Level of verbosity [0-4]
%		xRedundant      Current matrix with redundancy labels (see
%                       fGenRed.m for details)
%
%	Outputs:
%		xRedundant      Updated matrix with redundancy labels (see
%                       fGenRed.m for details)
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
% [2] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and
% redundancy classification in generalized process networks - II.
% Algorithms. Comp. Chem. Eng., 12(7), 689-703.
% [3] Villez, K. (2019). GENOBS* and GENRED* algorithms for observability
% and redundancy labeling.  (TR-008-01-0). Technical Report, Eawag,
% Duebendorf, Switzerland.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------


%% Redundancy Rule 3
% ------------------------------------------------------------------------
if verbose>=2,  disp('========================================================================================='); end
if verbose>=1,  disp('	REDUNDANCY RULE 3 '); end
if verbose>=2,  disp('========================================================================================='); end

xMeasured = System.xMeasured;
xKnownOrMeasured = System.xKnownOrMeasured;

%	Redundancy rule 3 in paper believed to be incorrect, as it results in
%	incorrect labelling of x4,4 as redundant in the example. This is
%	caused by assuming that the temporarily assigned measurements are
%	independent from the ones they are supposed to be redundant with. In
%	the example, x3,4 is assigned as measured in step (a). Then, the
%	cutset (3,4) is used later to label x4,4 as observable if following the
%	procedure.
%
%   Concept behind Rule 3 has been implemented in this manner:
%   Rules for temperature: temporary assignment is only possible through
%   splitter constraints therefore covered above with splitter rule.
%   Nothing added here
%   Rules for fraction: An alternative approach is to identify whether an
%   unmeasured fraction can be estimated independently on the basis of
%   distinct sets of information. When so, all measurements to
%   estimate this unmeasured fraction are redundant. One simple case occurs
%   when the same unmeasured fraction can be estimated independently from
%   the normalization rule and from an equal concentration rule. There
%   might be more complex cases but only this specific one is tested below.
%if verbose>=3,      xRedundant_old = xRedundant                 ;	end
xRedundant_old = xRedundant   ;


ii = System.varindex.ii ;
t = System.varindex.t ;
iit = [ ii t ];


% Assign temporary measurement based on normalization rule
xMeasurementNORM = xKnownOrMeasured;
xMeasurementNORM(:,:) = 0 ;
ii = System.varindex.ii ;
xMeasFraction = xKnownOrMeasured(:,ii) ;
xOneFractionNotMeasured = and(System.xArcPhysical,(sum(or(isnan(xMeasFraction),xMeasFraction==1),2)+1)==System.iNumberOfComponent) ;
xMeasurementNORM(xOneFractionNotMeasured,ii)= 1;
xMeasurementNORM(xKnownOrMeasured==1)=0; % Reset those measurement that were known before

% Assign temporary measurement based on equal fraction rule
xMeasurementEQUAL = xKnownOrMeasured;
xMeasurementEQUAL(:,:) = 0 ;
for s=1:size(System.xArcEqualComposition,2)

    iEdge = find(System.xArcEqualComposition(:,s)) ;
    for iComponent = iit(:)'

        xFractionMeasured = xKnownOrMeasured(iEdge,iComponent);
        if any(xFractionMeasured==1)
            iEdgeLabel = iEdge(~isnan(xKnownOrMeasured(iEdge,iComponent))) ;
            xMeasurementEQUAL(iEdgeLabel,iComponent)= 1;
        end
    end
end

xMeasurementEQUAL(xKnownOrMeasured==1)=0; % Reset those measurement that were known before

% index of flow/component combinations whose fraction could be estimated
% with the two independent methods:
[J,I]=find(and(xMeasurementEQUAL(:,ii),xMeasurementNORM(:,ii)));
for iCombo = 1:length(J)

    % 3a. Label measurements used in normalization rule to find this estimate:
    xMeas = xMeasured(J(iCombo),ii)==1 ;
    xRedundant(J(iCombo),ii(xMeas) ) = 1;

    % 3b. Label measurements used in equal fraction rule to find this estimate:
    % 3b(i) Find the set(s) of edges with concentrations equal to the
    % estimated concentration:
    xEdgeEqual = any(System.xEqualCompositionEdge(:,System.xEqualCompositionEdge(J(iCombo),:)),2);
    % 3b(ii) Execute labeling:
    xRedundant(and(xEdgeEqual,xMeasured(:,I(iCombo))==1),I(iCombo)) = 1;


end

if any( and(xRedundant_old(:)~=xRedundant(:),xRedundant(:)==1) )
    disp('Warning (!) - this piece of code was never tested on a positive case - may want to check whether this works as intended.')
end

if verbose>=3,	fDisplayLabelChanges(System,xRedundant_old,xRedundant),	end



end

