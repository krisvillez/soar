function xRedundant =  fGenRed(System,verbose)

% -------------------------------------------------------------------------
% SOAR toolbox - fGenRed.m
%
% This function implements the GENRED* algorithm used in [1], based on the
% GENRED algorithm in [2]. A description of GENRED* can be found in [3].
%
% Syntax: 	xRedundant =  fGenRed(System,verbose)
%
%	Inputs:
%		System          System description, including graph representation
%       verbose         Level of verbosity [0-4]
%
%	Outputs:
%		xRedundant      Matrix consisting of integers indicating the
%                       redundancy labels (1: redundant, -1:
%                       nonredundant, 0: no label, nan: variable is not
%                       measured; dimensions: number or arcs X number of
%                       variables)
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
% [2] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and
% redundancy classification in generalized process networks - II.
% Algorithms. Comp. Chem. Eng., 12(7), 689-703.
% [3] Villez, K. (2019). GENOBS* and GENRED* algorithms for observability
% and redundancy labeling.  (TR-008-01-0). Technical Report, Eawag,
% Duebendorf, Switzerland.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------


if verbose>=0,	disp('******* DO REDUNDANCY *******'); end

%%
% **************************************************************************************
% Rule 0 - Setup
% **************************************************************************************

xKnownOrMeasured =  System.xKnown ; % Label mass fraction
xKnownOrMeasured(System.xMeasured==1) = 1 ;

System.xKnownOrMeasured = xKnownOrMeasured;

xMeasured= System.xMeasured;
xRedundant = xMeasured; %Red;
xRedundant(xRedundant~=1) = nan;
xRedundant(xRedundant==1) = 0 ;

xMeasuredNotMassFlow = xMeasured(:,1:end-1) ;

if ~any(xMeasuredNotMassFlow(:))
    xRedundant = fGenRedRule2(System,verbose,xRedundant);
    xRedundant(xRedundant==0) = -1;
else
    %
    % **************************************************************************************
    % Rule 1-8
    % **************************************************************************************

    xRedundant = fGenRedRule1(System,verbose,xRedundant);
    xRedundant = fGenRedRule2(System,verbose,xRedundant);
    xRedundant = fGenRedRule3(System,verbose,xRedundant);
    xRedundant = fGenRedRule4(System,verbose,xRedundant);
    xRedundant = fGenRedRule5(System,verbose,xRedundant);
    xRedundant = fGenRedRule6(System,verbose,xRedundant);
    xRedundant = fGenRedRule7(System,verbose,xRedundant);
    xRedundant = fGenRedRule8(System,verbose,xRedundant);

end

end
