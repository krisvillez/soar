function strVarName = fGetVariableName(System,i,j)

% -------------------------------------------------------------------------
% SOAR toolbox - fGetVariableName.m
%
% This function computes string specifying the name of the variable in
% the row i and column j of the measurement/observability/redundancy
% matrices
%
% Syntax: 	strVarName = fGetVariableName(System,i,j)
%
%	Inputs: 	
%		System      System description
%       i           Row position
%       j           Column position
%
%	Outputs: 	
%       strVarName  Variable name as string
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2019-07-24
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2019 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


m	=	System.varindex.m	;
nn	=	System.varindex.nn	;
ii	=	System.varindex.ii	;
h	=	System.varindex.h	;
t	=	System.varindex.t	;


if ismember(j,ii),      strVarName = ['x' num2str(i) '_' num2str(j) ] ;
elseif ismember(j,nn),  strVarName = ['N' num2str(i) '_' num2str(j) ] ;
elseif ismember(j,t),   strVarName = ['T' num2str(i) ] ;
elseif ismember(j,h),   strVarName = ['H' num2str(i) ] ;
elseif ismember(j,m),   strVarName = ['M' num2str(i) ] ;
else,	error('Unknown variable type')
end
