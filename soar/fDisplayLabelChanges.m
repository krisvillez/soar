function fDisplayLabelChanges(System,xObservability_old,xObservability)

% -------------------------------------------------------------------------
% SOAR toolbox - fDisplayLabelChanges.m
%
% This function prints the changes of labels in the command line.
%
% Syntax: 	fDisplayLabelChanges(System,xObservability_old,xObservability)
%
%	Inputs: 	
%		System                  System description
%       xObservability_old      Former observability labels
%       xObservability          Current observability labels
%
%	Outputs: 	/
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2019-07-24
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2019 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

m = System.varindex.m;
nn = System.varindex.nn;
ii = System.varindex.ii;
h = System.varindex.h;
t = System.varindex.t;

[J,I]=find(and(xObservability_old~=xObservability,xObservability==1)');

ism = ismember(J,m) ;
if any(ism)
    str = '     (+) Mass flow: ' ;
    ism=find(ism);
    for r=ism(:)'
        str = [ str 'M' num2str(I(r)) ' '  ] ;
    end
    disp(str)
end

ism = ismember(J,nn) ;
if any(ism)
    str = '     (+) Component flow: ' ;
    ism=find(ism);
    for r=ism(:)'
        str = [ str 'N' num2str(I(r)) '_' num2str((ii(nn==J(r)))) ' ' ] ;
    end
    disp(str)
end

ism = ismember(J,ii) ;
if any(ism)
    str = '     (+) Component fraction: ' ;
    ism=find(ism);
    for r=ism(:)'
        str = [ str 'x' num2str(I(r)) '_' num2str(J(r)) ' ' ] ;
    end
    disp(str)
end


ism = ismember(J,h) ;
if any(ism)
    str = '     (+) Heat flow: ' ;
    ism=find(ism);
    for r=ism(:)'
        str = [ str 'H' num2str(I(r)) ' '  ] ;
    end
    disp(str)
end

ism = ismember(J,t) ;
if any(ism)
    str = '     (+) Temperature: ' ;
    ism=find(ism);
    for r=ism(:)'
        str = [ str 'T' num2str(I(r))  ' ' ] ;
    end
    disp(str)
end




[J,I]=find(and(xObservability_old~=xObservability,xObservability==-1)');

ism = ismember(J,m) ;
if any(ism)
    str = '     (-) Mass flow: ' ;
    ism=find(ism);
    for r=ism(:)'
        str = [ str 'M' num2str(I(r)) ' '  ] ;
    end
    disp(str)
end

ism = ismember(J,h) ;
if any(ism)
    str = '     (-) Heat flow: ' ;
    ism=find(ism);
    for r=ism(:)'
        str = [ str 'H' num2str(I(r)) ' '  ] ;
    end
    disp(str)
end

ism = ismember(J,t) ;
if any(ism)
    str = '     (-) Temperature: ' ;
    ism=find(ism);
    for r=ism(:)'
        str = [ str 'T' num2str(I(r))  ' ' ] ;
    end
    disp(str)
end


ism = ismember(J,ii) ;
if any(ism)
    str = '     (-) Component fraction: ' ;
    ism=find(ism);
    for r=ism(:)'
        str = [ str 'x' num2str(I(r)) '_' num2str(J(r)) ' ' ] ;
    end
    disp(str)
end

ism = ismember(J,nn) ;
if any(ism)
    str = '     (-) Component flow: ' ;
    ism=find(ism);
    for r=ism(:)'
        str = [ str 'N' num2str(I(r)) '_' num2str((ii(nn==J(r)))) ' ' ] ;
    end
    disp(str)
end