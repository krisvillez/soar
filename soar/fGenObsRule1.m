function xObservable = fGenObsRule1(System,verbose,xObservable)

% -------------------------------------------------------------------------
% SOAR toolbox - fGenObsRule1.m
%
% This function implements rule 1 of the GENOBS* algorithm [1-3].
%
% Syntax: 	xObservable = fGenObsRule1(System,verbose,xObservable)
%
%	Inputs:
%		System          System description, including graph representation
%       verbose         Level of verbosity [0-4]
%		xObservable     Current matrix with observability labels (see
%                       fGenObs.m for details)
%
%	Outputs:
%		xObservable     Updated matrix with observability labels (see
%                       fGenObs.m for details)
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
% [2] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and
% redundancy classification in generalized process networks - II.
% Algorithms. Comp. Chem. Eng., 12(7), 689-703.
% [3] Villez, K. (2019). GENOBS* and GENRED* algorithms for observability
% and redundancy labeling.  (TR-008-01-0). Technical Report, Eawag,
% Duebendorf, Switzerland.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------


iNumberOfComponent = System.iNumberOfComponent;
xArcPhysical = System.xArcPhysical ;

%% Rule 1
% ------------------------------------------------------------------------
if verbose>=2,  disp('========================================================================================='); end
if verbose>=1,  disp('	OBSERVABILITY RULE 1 '); end
if verbose>=2,  disp('========================================================================================='); end


xRuleOneConverged = false;
iRuleOneIterations = 0 ;

while ~xRuleOneConverged

    xObservable_before	=	xObservable              ;
    iRuleOneIterations	=	iRuleOneIterations +1	;

    %% Rule 1a
    % --------------------------------------------------------------------------------------
    if verbose>=3,      xObservable_old	=   xObservable                  ;	end
    if verbose>=2,      disp('	Rule 1a         Fraction normalization rule')   ;	end
    xObservable	=	fRuleNormalization(System,xObservable)	;
    if verbose>=3,      fDisplayLabelChanges(System,xObservable_old,xObservable),	end
    % --------------------------------------------------------------------------------------

    %% Rule 1b
    % --------------------------------------------------------------------------------------
    if verbose>=3,      xObservable_old	=   xObservable                  ;	end
    if verbose>=2,      disp('	Rule 1b         Splitter rule')                 ;   end
    xObservable	=	fRuleSplitter(System,xObservable) ;
    if verbose>=3,      fDisplayLabelChanges(System,xObservable_old,xObservable),	end
    % --------------------------------------------------------------------------------------

    %% Rule 1c(i)
    % --------------------------------------------------------------------------------------
    if verbose>=3,      xObservable_old	=   xObservable                  ;	end
    if verbose>=2,      disp('	Rule 1c(i)      Heat flow balance')             ;   end

    xObservableTemperature = xObservable(:,System.varindex.t);
    if any(xObservableTemperature==1)
        xSelect = and(sum(System.Acut~=0,2)==2,~any(System.Acut(:,~xArcPhysical),2)) ;
        CutSets2 = System.Acut(xSelect,:);

        for iCut=1:size(CutSets2,1)
            iFlow       =	find(CutSets2(iCut,:))          ;
            xLabel      =	xObservableTemperature(iFlow)	;
            xObservable =	and(~isnan(xLabel),xLabel==1)	;
            if any(xObservable)
                xObservable(iFlow,t)	=	1               ;
            end
        end
    end

    if verbose>=3,      fDisplayLabelChanges(System,xObservable_old,xObservable),	end
    % --------------------------------------------------------------------------------------

    %% Rule 1c(ii)
    % --------------------------------------------------------------------------------------
    if verbose>=3,      xObservable_old	=   xObservable                  ;	end
    if verbose>=2,	disp('	Rule 1c(ii)     Component flow balance'); end

    CutSets2            =	System.AcutMassEffect(sum(System.AcutMassEffect~=0,2)==2,:) ;

    for iComponent=1:iNumberOfComponent
        xObservableCompFrac = xObservable(:,iComponent);
        if any(xObservableCompFrac==1)
            for iCut=1:size(CutSets2,1)
                iFlow       =	find(CutSets2(iCut,:))              ;
                xLabel      =	xObservableCompFrac(iFlow)	;
                xFracObservable =	and(~isnan(xLabel),xLabel==1)       ;
                if any(xFracObservable)
                    xObservable(iFlow,iComponent) = 1;
                end
            end
        end
    end

    if verbose>=3,      fDisplayLabelChanges(System,xObservable_old,xObservable),	end
    % --------------------------------------------------------------------------------------

    %% Rule 1d
    % --------------------------------------------------------------------------------------
    if verbose>=3,      xObservable_old	=   xObservable                  ;	end
    if verbose>=2,	disp('	Rule 1d     	Check convergence'); end

    if all( ~and(xObservable_before(:)~=xObservable(:),~isnan(xObservable(:))) )
        xRuleOneConverged = true ;
    end
    % --------------------------------------------------------------------------------------

end

    %% Rule 1e
    % --------------------------------------------------------------------------------------
    if verbose>=3,	xObservable_old =xObservable; end
    if verbose>=2,	disp('	Rule 1e         Bilinear assignment'); end
    xObservable	=	fAssignBilinear(System,xObservable)	;
    if verbose>=3,      fDisplayLabelChanges(System,xObservable_old,xObservable),	end
    % --------------------------------------------------------------------------------------


end

