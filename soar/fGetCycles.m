function Cycles = fGetCycles(A)

% -------------------------------------------------------------------------
% SOAR toolbox - fGetCycles.m
%
% This function computes all the cycles in a (directed) graph.
%
% Syntax: 	Cycles = fGetCycles(A)
%
%	Inputs: 	
%		A       Incidence matrix of a directed graph
%
%	Outputs: 	
%		Cycles	All cycles in the graph, specified as a matrix consisting
%               of booleans (0: arc is not in cycle, 1: arc is in cycle;
%               dimensions: #cycles x #arcs in original graph)
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2019-07-24
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2019 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

J = size(A,2);
E = nan(J,2);
for j=1:J
    E(j,1) = find(A(:,j)==-1);
    E(j,2) = find(A(:,j)==+1);
end

% 1. fundamental cycles

% search fundamental cycles in graph where 2-arc loops have been removed.
% Otherwise one receives erroneous results.
[Eu,index] = unique(sort(E,2),'rows');
Cu = grCycleBasis(Eu);
C = zeros(size(E,1),size(Cu,2)) ;
C(index,:) = Cu;
C = C(:,any(C,1));

% 2. loops based on pairs of edges
E2=sort(E,2);
[~,~,sets]=unique(E2,'rows');

Cpair = false(size(C,1),0);
for set=unique(sets)'
    if sum(set==sets)>=2
        index = find(set==sets) ;
        %pairs = combnk(,2);
        pairs = fCombn2(index);
        for iPair = 1:size(pairs,1)
            c_ = ismember( (1:size(C,1))' , pairs(iPair,:) );
            Cpair = [Cpair c_ ];
        end
    end
end
C = [ C Cpair ];

% 3. Enumerate all cycles by XOR operation on each pair of cycles
Finished = false;
Iterations = 0 ;
while ~Finished
    Iterations = Iterations+1 ;
    Cold = C ;
    nCycle = size(C,2);
    Cnew = zeros(size(C,1),nCycle^2);
    c_index = 0;
    for c1=1:(nCycle-1)
        for c2=(c1+1):nCycle
             inclusion1 = all(C(C(:,c1)==1,c2));
             inclusion2 = all(C(C(:,c2)==1,c1));
            overlap = and(C(:,c1),C(:,c2));
            if ~or(inclusion1,inclusion2) && ( ~all(overlap) && any(overlap) ) % at least one edge in common -> apply
                c_index =c_index+1;
                Cnew(:,c_index) = xor(C(:,c1),C(:,c2));
            end
        end
    end
    Cnew = Cnew(:,1:c_index);
    C= [C Cnew];
    C= sortrows(unique(C','rows'))';
    if size(Cold,2)==size(C,2) && all(C(:)==Cold(:))
        Finished = true ;
    end
end


Cycles= C;
%disp(['Iterations: ' num2str(Iterations)])
