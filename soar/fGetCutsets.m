function Cutsets = fGetCutsets(A)

% -------------------------------------------------------------------------
% SOAR toolbox - fGetCycles.m
%
% This function computes all the cutsets in a (directed) graph.
%
% Syntax: 	Cutsets = fGetCutsets(A)
%
%	Inputs:
%		A       Incidence matrix of a directed graph
%
%	Outputs:
%		Cutsets	All cutsets in the graph, specified as a matrix consisting
%               of booleans (0: arc is not in cutset, 1: arc is in cutset;
%               dimensions: #cutsets x #arcs in original graph)
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------


Acut =A;

% 1. Enumerate all cycles by XOR operation on each pair of nodel balances
Finished = false;
Iterations = 0 ;
while ~Finished
    Iterations = Iterations+1 ;
    Acut_old = Acut ;
    nCut = size(Acut,1);

    Acut_new = zeros(nCut^2,size(Acut,2));
    c_index = 0;
    for c1=1:(nCut-1)
        for c2=(c1+1):nCut
            overlap = (Acut(c1,:)~=0)==(Acut(c2,:)~=0);
            %overlap
            %Acut([c1 c2],:)
            if ~all(overlap) && any(overlap) % at least one edge in common -> apply
                c_index =c_index+1;
                if all(Acut(c1,overlap)==Acut(c2,overlap))
                    a_ = round(Acut(c1,:)-Acut(c2,:));
                elseif all(Acut(c1,overlap)==-Acut(c2,overlap))
                    a_ = round(Acut(c1,:)+Acut(c2,:));
                end
                index= find(a_~=0,1,'first');
                Acut_new(c_index,:) = a_*sign(a_(index)) ;
            end
        end
    end
    Acut_new = Acut_new(1:c_index,:);
    Acut_new = Acut_new(any(Acut_new~=0,2),:);
    Acut= [Acut ; Acut_new];
    Acut= sortrows(unique(Acut,'rows'));
    if size(Acut_old,1)==size(Acut,1) && all(Acut_old(:)==Acut(:))
        Finished = true ;
    end
end

Cutsets=Acut;

return
