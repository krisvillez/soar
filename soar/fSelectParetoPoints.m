function xInclude = fSelectParetoPoints(criteria)

% -------------------------------------------------------------------------
% SOAR toolbox - fParetoSearch.m
%
% This function identifies the Pareto optimal points in a set of
% N-dimensional points
%
% Syntax: xInclude = fSelectParetoPoints(criteria)
%
%	Inputs:
%		criteria    MxN-dimensional matrix of points with rows specifying
%                   the points and columns corresponding to the
%                   optimization criteria. It is assumed that all criteria
%                   must be minimized.
%
%	Outputs:
%       xInclude    M-dimensional boolean vector indicating whether the
%                   points are on (1) or off (0) the Pareto front
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

[nSample,nCriterion] = size(criteria) ;

Dominated = nan(nSample,nSample,nCriterion);
DominatedOrEqual = nan(nSample,nSample,nCriterion);
for iCriterion=1:nCriterion

    Dominated(:,:,iCriterion) = criteria(:,iCriterion)>criteria(:,iCriterion)'  ;
    DominatedOrEqual(:,:,iCriterion) = criteria(:,iCriterion)>=criteria(:,iCriterion)'  ;

end

xInclude = true(nSample,1);
for iCriterion=1:nCriterion

    xChange = any(all(cat(3,DominatedOrEqual(:,:,setdiff(1:nCriterion,iCriterion)),Dominated(:,:,iCriterion) ),3),2);

    xInclude(xChange) = false ;
end

