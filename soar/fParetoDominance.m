function xInclude = fParetoDominance(CritLB,CritUB)

% -------------------------------------------------------------------------
% SOAR toolbox - fParetoDominance.m
%
% This function identifies the solutions sets, which cannot be excluded based
% on Pareto dominance.
%
% Syntax: 	xInclude = fParetoDominance(CritLB,CritUB)
%
%	Inputs:
%		CritLB          Matrix of lower bound values, rows are sets and
%                       columns are objectives
%		CritUB          Matrix of upper bound values, with entries
%                       corresponding to entries of CritLB
%
%	Outputs:
%		xInclude        Vector of booleans, row dimension matching row
%                       dimensions of CritLB and CritUB. True = Not
%                       excluded from Pareto front. False = Provably
%                       excluded from Pareto front
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
% [2] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and
% redundancy classification in generalized process networks - II.
% Algorithms. Comp. Chem. Eng., 12(7), 689-703.
% [3] Villez, K. (2019). GENOBS* and GENRED* algorithms for observability
% and redundancy labeling.  (TR-008-01-0). Technical Report, Eawag,
% Duebendorf, Switzerland.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

[nBranch,nCrit] = size(CritLB) ;
[nRef,nCrit1]  = size(CritUB) ;

assert(nCrit==nCrit1,'column dimension should be equal')

Dominated = nan(nBranch,nRef,nCrit);
DominatedOrEqual = nan(nBranch,nRef,nCrit);
for iCrit=1:nCrit
    LB_ = repmat(CritLB(:,iCrit),[1 nRef]) ;
    UB_ = repmat(CritUB(:,iCrit),[1 nBranch]) ;
    Dominated(:,:,iCrit) = LB_>UB_'  ;
    DominatedOrEqual(:,:,iCrit) = LB_>=UB_'  ;
end

xInclude = true(nBranch,1);
for iRef=1:nRef
    for iCrit=1:nCrit
        xChange = any(all(cat(3,DominatedOrEqual(:,iRef,setdiff(1:nCrit,iCrit)),Dominated(:,iRef,iCrit) ),3),2);
        xInclude(xChange) = false ;
    end
end
