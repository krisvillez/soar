function xObservable = fAssignMassFlow(System,xObservable)

% -------------------------------------------------------------------------
% SOAR toolbox - fAssignMassFlow.m
%
% This function labels flow rates as observable on the basis of observable
% flow rates and hydraulic balance equations.
%
% Syntax: 	xObservable = fAssignMassFlow(System,xObservable)
%
%	Inputs:
%		System          System description, including graph representation
%		xObservable     Current matrix with observability labels (see
%                       fGenObs.m for details)
%
%	Outputs:
%		xObservable     Updated matrix with observability labels (see
%                       fGenObs.m for details)
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

xArcEnergy         =	System.xArcEnergy          ;
xArcReaction       =	System.xArcReaction        ;

m = System.varindex.m ;

CyclesAll                       =	System.CyclesAll;
CyclesPhysicalPipes             =	CyclesAll(:, ~any( CyclesAll(or(xArcEnergy,xArcReaction),:),1 ) );
CyclesPhysicalPipesUnmeasured	=	CyclesPhysicalPipes(:, ~any(CyclesPhysicalPipes(xObservable(:,m)==1,:),1)  ) ;
xLabelObservable                =	and(~or(xArcEnergy,xArcReaction),~any(CyclesPhysicalPipesUnmeasured,2)) ;
xObservable(xLabelObservable,m) =	true                ;
