function xObservable =  fGenObsRule2(System,verbose,xObservable)

% -------------------------------------------------------------------------
% SOAR toolbox - fGenObsRule2.m
%
% This function implements rule 2 of the GENOBS* algorithm [1-3].
%
% Syntax: 	xObservable = fGenObsRule2(System,verbose,xObservable)
%
%	Inputs:
%		System          System description, including graph representation
%       verbose         Level of verbosity [0-4]
%		xObservable     Current matrix with observability labels (see
%                       fGenObs.m for details)
%
%	Outputs:
%		xObservable     Updated matrix with observability labels (see
%                       fGenObs.m for details)
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
% [2] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and
% redundancy classification in generalized process networks - II.
% Algorithms. Comp. Chem. Eng., 12(7), 689-703.
% [3] Villez, K. (2019). GENOBS* and GENRED* algorithms for observability
% and redundancy labeling.  (TR-008-01-0). Technical Report, Eawag,
% Duebendorf, Switzerland.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------


%iNumberOfComponent	=	System.iNumberOfComponent	;
m                   =	System.varindex.m           ;
ii                  =	System.varindex.ii          ;
t                  =	System.varindex.t          ;
xArcEnergy         =	System.xArcEnergy          ;
xArcReaction       =	System.xArcReaction        ;
xArcSplitter       =	System.xArcSplitter        ;

if verbose>=2,  disp('========================================================================================='); end
if verbose>=1,  disp('	OBSERVABILITY RULE 2 '); end
if verbose>=2,  disp('========================================================================================='); end

if verbose>=3,	xObservable_old	=   xObservable	;	end

if any(xObservable(:,m)==0)

    xPureComponentFlow	=	sum(~isnan(xObservable(:,ii)),2)==1                                                              ;
    Cycles_Grse         =	System.CyclesAll(:,~any(System.CyclesAll(any([ xArcEnergy xArcReaction xArcSplitter  ],2),:),1) );
    Cycles_Grse_m       =	Cycles_Grse(:,~any(Cycles_Grse(xObservable(:,m)==1,:),1))                                        ;

    Cycles_Grse_mx      =	Cycles_Grse_m(:,~any(Cycles_Grse_m( and(any(xObservable(:,ii)==1,2),~xPureComponentFlow),:),1) )	;
    nCycle              =	size(Cycles_Grse_mx,2)                                                                              ;

    if nCycle>0
        for iCycle=1:nCycle

            xCycle      =	Cycles_Grse_mx(:,iCycle)==1    ;

            if sum(xObservable(xCycle,t))<=1 % Rule 2(i)
                if any(all(~isnan(xObservable(xCycle==1,ii)),1)) % Rule 2(ii)

                    xObservable(xCycle,m)	=	-1  ;
                end
            end
        end

    end
end

if verbose>=3,      fDisplayLabelChanges(System,xObservable_old,xObservable),	end

end

