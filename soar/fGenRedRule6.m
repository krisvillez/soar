function xRedundant = fGenRedRule6(System,verbose,xRedundant)

% -------------------------------------------------------------------------
% SOAR toolbox - fGenRedRule6.m
%
% This function implements rule 6 of the GENRED* algorithm [1-3].
%
% Syntax: 	xRedundant = fGenRedRule6(System,verbose,xRedundant)
%
%	Inputs:
%		System          System description, including graph representation
%       verbose         Level of verbosity [0-4]
%		xRedundant      Current matrix with redundancy labels (see
%                       fGenRed.m for details)
%
%	Outputs:
%		xRedundant      Updated matrix with redundancy labels (see
%                       fGenRed.m for details)
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
% [2] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and
% redundancy classification in generalized process networks - II.
% Algorithms. Comp. Chem. Eng., 12(7), 689-703.
% [3] Villez, K. (2019). GENOBS* and GENRED* algorithms for observability
% and redundancy labeling.  (TR-008-01-0). Technical Report, Eawag,
% Duebendorf, Switzerland.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------


%% Redundancy Rule 6
% ------------------------------------------------------------------------
if verbose>=2,  disp('========================================================================================='); end
if verbose>=1,  disp('	REDUNDANCY RULE 6 '); end
if verbose>=2,  disp('========================================================================================='); end

if verbose>=3,      xRedundant_old = xRedundant                 ;	end

xMeasured= System.xMeasured;
xKnownOrMeasured = System.xKnownOrMeasured;
%	--------------
%       6(a/c)
%	--------------

ii = System.varindex.ii ;
m = System.varindex.m ;
t = System.varindex.t ;
% 6(a)(i) Arcs with end nodes in different components of Grse_mx
% = Edges on cutsets composed of arcs that are reaction edges, splitter
% arcs, energy arcs, arcs with a mass flow measurement, or arcs that are
% not pure arcs and have at least one fraction measurement
xFractionMeasured = and(~System.xArcPure,any(xKnownOrMeasured(:,ii)==1,2));
xArcsRemoved = any([ System.xArcReaction any(System.xArcSplitter,2) System.xArcEnergy xKnownOrMeasured(:,m) xFractionMeasured ],2) ;

% 6(a)(ii) Cycles with at least one component in common and at most one
% temperature measurement
nCycle = size(System.CyclesAll,2);
Cycles_t1 = System.CyclesAll(:,sum(and(repmat(xKnownOrMeasured(:,t),[1 nCycle]),System.CyclesAll),1)<=1) ;

for v=[ m ii ]
    if v==m
        xCandidate = all([ xRedundant(:,v)==0 ~System.xArcSplitter or(~any(xKnownOrMeasured(:,ii)==1,2),System.xArcPure) ],2);
    else
        xCandidate = all([ xRedundant(:,v)==0 ~System.xArcSplitter ~xKnownOrMeasured(:,m) xRedundant(:,setdiff(ii,v))~=-1 ~System.xArcPure ],2);
    end

    iCandidate = find(xCandidate);

    for iEdge = iCandidate(:)'
        xArcsP = xArcsRemoved;
        xArcsP(iEdge) = 0;
        Cycles = Cycles_t1(:,~any(Cycles_t1(xArcsP,:),1)) ;
        if any(Cycles(iEdge,:))
            Cycles = Cycles(:,Cycles(iEdge,:));
            nCycle = size(Cycles,2);
            iCycle=0;
            while iCycle<nCycle
                iCycle = iCycle+1;
                if any(all(~isnan( xKnownOrMeasured(Cycles(:,iCycle)==1,ii) ),1),2) % at least one component in common
                    xRedundant(iEdge,v) = -1;
                    iCycle = nCycle; % -> quit while loop
                end
            end
        end
    end

end

%	--------------
%        6(b)
%	--------------

xCandidate = and(xMeasured(:,t)==1,xRedundant(:,t)==0) ;
Cycles_rsemx = System.CyclesAll(:,~any(System.CyclesAll(xArcsRemoved,:),1));
nCycle = size(Cycles_rsemx,2);
Cycles_t2 = Cycles_rsemx(:,sum(and(repmat(xKnownOrMeasured(:,t),[1 nCycle]),Cycles_rsemx),1)<=2) ;

if any(Cycles_t2(xCandidate,:),2)
    Cycles_t2 = Cycles_t2(:,any(Cycles_t2(xCandidate,:),2));
    nCycle = size(Cycles_t2,2);
    iCycle=0;
    while iCycle<nCycle
        iCycle = iCycle+1;
        if any(all(~isnan( xKnownOrMeasured(Cycles_t2(:,iCycle)==1,ii) ),1),2) % at least one component in common
            xRedundant(iEdge,t) = -1;
            iCycle = nCycle; % -> quit while loop
        end
    end
end

if verbose>=3,	fDisplayLabelChanges(System,xRedundant_old,xRedundant),	end


end

