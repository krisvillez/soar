function xObservable = fAssignBilinear(System,xObservable)

% -------------------------------------------------------------------------
% SOAR toolbox - fAssignBilinear.m
%
% This function labels variables as observable on the basis of the bilinear
% relationships between component flow rate (N), total flow rate (M), and
% mass fraction (x): N = M x . Similarly applied for heat exchange,
% reactions, and storage.
%
% Syntax: 	xObservable = fAssignBilinear(System,xObservable)
%
%	Inputs:
%		System          System description, including graph representation
%		xObservable     Current matrix with observability labels (see
%                       fGenObs.m for details)
%
%	Outputs:
%		xObservable     Updated matrix with observability labels (see
%                       fGenObs.m for details)
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------


m  = System.varindex.m ;
iit = [System.varindex.ii System.varindex.t ];
nnh = [System.varindex.nn System.varindex.h ];
C = length(iit);

% M * x -> N
% M * t -> H
xObservableIntensive = xObservable(:,iit) ;
if any(xObservableIntensive(:)==1)
    ObsFlow = xObservable(:,m);
    ObsFraction = xObservable(:,iit);
    ObsComponentFlow = xObservable(:,nnh);
    NanPattern = ObsComponentFlow;
    NanPattern(~isnan(NanPattern))=1;
    ObsComponentFlowNew =ObsFraction.*repmat(ObsFlow,[1 C]);

    ObsComponentFlow(isnan(ObsComponentFlow)) = 0;
    ObsComponentFlowNew(isnan(ObsComponentFlowNew)) = 0;
    ObsComponentFlow = or(ObsComponentFlow==1,ObsComponentFlowNew==1);

    xObservable(:,nnh) = ObsComponentFlow.*NanPattern;
end

% N/M  -> x
% H/M  -> t
xObservableExtensive = xObservable(:,nnh)==1 ;
if any(xObservableExtensive(:)==1)

    ObsFlow = xObservable(:,m);
    ObsComponentFlow = xObservable(:,nnh);
    ObsFraction = xObservable(:,iit);
    NanPattern = ObsFraction;
    NanPattern(~isnan(NanPattern))=1;
    ObsFractionNew =ObsComponentFlow.*repmat(ObsFlow,[1 C]);

    ObsFraction(isnan(ObsFraction)) = 0;
    ObsFractionNew(isnan(ObsFractionNew)) = 0;
    ObsFraction = or(ObsFraction==1,ObsFractionNew==1);

    xObservable(:,iit) = ObsFraction.*NanPattern;
end

% % N/x  -> M
% % H/t  -> M
if any(xObservableExtensive(:)==1) && any(xObservableIntensive(:)==1)
    xObsFraction = xObservable(:,iit);
    xObsComponentFlow = xObservable(:,nnh);
    xObsRate = xObservable(:,m);
    for iComponent = 1:length(iit)
        xObsFractionColumn = and(~isnan(xObsFraction(:,iComponent)),xObsFraction(:,iComponent)==1);
        xObsComponentFlowColumn= and(~isnan(xObsComponentFlow(:,iComponent)),xObsComponentFlow(:,iComponent)==1);
        xExistRate = ~isnan(xObsRate) ;
        xObsRate = all([ xExistRate, xObsFractionColumn, xObsComponentFlowColumn ],2);
        xObservable(xObsRate,m) = 1;
    end
end
