function [bubble_position,xSurplus,iBubbleSize,NB,NF]=fFigurePareto(iObjectives,strName)

% -------------------------------------------------------------------------
% SOAR toolbox - fFigurePareto.m
%
% This function makes a figure showing the Pareto front.
%
% Syntax: 	[bubble_position,xSurplus,iBubbleSize,NB,NF]=fFigurePareto(iObjectives,strName)
%
%	Inputs:
%		iObjectives     Objective function values for Pareto optimal
%                       solutions. Row dimension = number of solutions;
%                       Column dimension:  umber of optimization criteria
%       strName         String for figure title
%
%	Outputs:
%		bubble_position     unique positions of the Pareto optimal
%                           solutions on the Pareto front
%       xSurplus            Boolean vector indicating whether Pareto point
%                           is for solutions with (true) or without (false)
%                           surplus redundancy, with length matching row
%                           dimension of bubble_position
%       iBubbleSize         Number of Pareto optimal solutions in Pareto
%                           front position, with length matching row
%                           dimension of bubble_position
%       NB                  Number of unique positions on the Pareto front
%       NF                  Number of solutions on the Pareto front
%
% References:
% [1] Villez, K., Vanrolleghem, P. A., Corominas, Ll. (2020).
% Optimal placement of flow rate and concentration sensors on wastewater
% treatment plants. Comp. Chem. Eng., 139, 106880.
% [2] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and
% redundancy classification in generalized process networks - II.
% Algorithms. Comp. Chem. Eng., 12(7), 689-703.
% [3] Villez, K. (2019). GENOBS* and GENRED* algorithms for observability
% and redundancy labeling.  (TR-008-01-0). Technical Report, Eawag,
% Duebendorf, Switzerland.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

nCriterion = size(iObjectives,2);

iObjectives    =   (fliplr(sortrows(fliplr(iObjectives))));

[bubble_position,ix,iy]=unique(iObjectives,'rows');

nBubble = size(bubble_position,1) ;
iBubbleSize = nan(nBubble,1);
for iBubble=1:nBubble
    iBubbleSize(iBubble) = sum(iy==iBubble);
end

nObservableMax=max(bubble_position(:,2));
nCostMax=max(bubble_position(:,1));

xSurplus = false(nBubble,1);
if nCriterion>=3
    nRedundantMax=max(bubble_position(:,3));

    ObsUnique = unique(bubble_position(:,2));
    for iObs=ObsUnique(:)'
        xInclude = bubble_position(:,2)==iObs;
        iDiff = bubble_position(:,3)-bubble_position(:,1) ;
        iDiffMax=max(iDiff(xInclude));
        xInclude = and(xInclude,iDiff==iDiffMax) ;
        xCostMin = min(bubble_position(xInclude,1));
        xInclude = and(xInclude,bubble_position(:,1)>xCostMin) ;
        xSurplus(xInclude) = true ;
    end
end


figure('Name',strName,'NumberTitle','off'), hold on
grid on
xFront = true(nBubble,1) ;

for iBubble=1:nBubble
    MarkerSize = 5+20*sqrt((iBubbleSize(iBubble))/max(iBubbleSize));
    if xSurplus(iBubble)
        colour = 'r';
    else
        if bubble_position(iBubble,1)==0
            colour = 'k';
        elseif bubble_position(iBubble,2)==nObservableMax
            if nCriterion>=3 && bubble_position(iBubble,3)>0
                colour = 'y' ;
            else
                colour = 'b' ;
            end
        else
            if nCriterion>=3 && bubble_position(iBubble,3)>0
                colour = 'g' ;
            else
                colour = 'w' ;
            end
        end
    end
    switch nCriterion
        case 2
            h = plot(bubble_position(iBubble,1),bubble_position(iBubble,2),'ko','MarkerFaceColor',colour,'MarkerSize', MarkerSize);
        case 3
            h = plot3(bubble_position(iBubble,1),bubble_position(iBubble,2),bubble_position(iBubble,3),'ko','MarkerFaceColor',colour,'MarkerSize', MarkerSize);
            plot3(bubble_position(iBubble,1)*ones(1,2),bubble_position(iBubble,2)*ones(1,2),[bubble_position(iBubble,3) 0],'k-')
        case 4
            h = plot3(bubble_position(iBubble,1),bubble_position(iBubble,2),bubble_position(iBubble,4)./max(bubble_position(iBubble,1),1),'ko','MarkerFaceColor',colour,'MarkerSize', MarkerSize);
            plot3(bubble_position(iBubble,1)*ones(1,2),bubble_position(iBubble,2)*ones(1,2),[bubble_position(iBubble,4)./max(bubble_position(iBubble,1),1) 0],'k-')
    end

end
switch nCriterion
    case 2
        xlabel('f^C')
        ylabel('f^O')
    case {3,4}
        xlabel('f^C')
        ylabel('f^O')
        set(gca,'Xlim',[0 nCostMax+0.5],'Xtick',0:nCostMax)
        set(gca,'Ylim',[0 nObservableMax+0.5],'Ytick',0:nObservableMax)
        XTL = char(get(gca,'Xticklabel'));
        steps = max(round(nCostMax/10),1);
        XTL(setdiff(1:length(XTL),1:steps:end),:)=' ';
        set(gca,'Xticklabel',XTL)
        YTL = char(get(gca,'Yticklabel'));
        steps = max(round(nObservableMax/10),1);
        YTL(setdiff(1:length(YTL),1:steps:end),:)=' ';
        set(gca,'Yticklabel',YTL)

        if nCriterion==3
            zlabel('f^R')
            set(gca,'Zlim',[0 nRedundantMax],'Ztick',0:nRedundantMax)

            plot3(0,nObservableMax,nRedundantMax,'kv','MarkerFaceColor','w');
            plot3([0 0],[0 nObservableMax],[nRedundantMax nRedundantMax],'k--')
            plot3([0 0],[nObservableMax nObservableMax],[0 nRedundantMax],'k--')
            plot3([0 nCostMax],[nObservableMax nObservableMax],[nRedundantMax nRedundantMax],'k--')
        else
            zlabel('f^{C,q}/f^C')
            set(gca,'Zlim',[0 1],'Ztick',0:0.1:1)
            nRedundantMax =1;
        end
end

NB = (sum((xFront))) ;
NF = (sum(iBubbleSize(xFront))) ;
disp(['Bubbles without surplus redundancy: ' num2str( sum((~xSurplus)))])
disp(['Number of layouts without surplus redundancy: ' num2str(sum(iBubbleSize(~xSurplus)))])
M = [bubble_position(~xSurplus,:) iBubbleSize(~xSurplus) ];
M1 = fliplr(M(:,1:3));
[~,index] = sortrows(M1);
disp(M(index,:))
disp(['Bubbles with surplus redundancy: ' num2str( sum((xSurplus)))])
disp(['Number of layouts with surplus redundancy: ' num2str(sum(iBubbleSize(xSurplus)))])
M = [bubble_position(xSurplus,:) iBubbleSize(xSurplus) ];
[~,index] = sortrows(fliplr(M(:,1:3)));
disp(M(index,:))

if nCriterion>=3
    view(-135,30)
end
drawnow()
