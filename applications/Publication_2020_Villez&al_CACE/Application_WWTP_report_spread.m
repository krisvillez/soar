clc
clear all
close all

verbose     =	0	;

addpath('../../graphtheory/')
addpath('../../soar/')
addpath('../../support/')
global OneComponent
global RedundancyDeficit

OneComponent = true ;
RedundancyDeficit = false ;

Count = 0 ;
iTabrow = 0;
MatrixOverview = cell(0);
for iSystemID	= [ 10:12 20:23  ]
    for iObj=1:2
        for nCriteria=2:3
            iTabrow  = iTabrow +1;

            lstCrit = 1:nCriteria;

            strProblem = ['Front_' num2str(iSystemID) '_' num2str(iObj)  '_' num2str(nCriteria)]	;

            switch iObj
                case 1
                    strObjective = 'FlowOnly';
                case 2
                    strObjective = 'FlowAndComponent1';
            end

            clear System
            clear Solutions
            clear iObjectives
            clear xObservable

             load(['.\result\res_Sys' num2str(iSystemID) '_Crit' num2str(nCriteria) '_Obj' strObjective] )
%             seconds(1) = time1-time0;

            nSensor = sum(System.xSensorCandidate(:)) ;
            nLayout = 2.^sum(System.xSensorCandidate(:)) ;
            Solutions = iTree(:,1);
            nLayoutChecked = sum(all(~isnan(criteria),2)) ;

            [bubble_position,xExcess,iBubbleSize,NB1,nLayoutPareto] = fFigurePareto(criteria(Solutions+1,:),strProblem);
            drawnow()

            load(['.\result\resext_Sys' num2str(iSystemID) '_Crit' num2str(nCriteria) '_Obj' strObjective] )

%             seconds(2) = time3;

            nLayoutCheckedExcess = sum(all(~isnan(criteria),2)) ;

%             timeTotal = seconds(1);
%             timeTotalExcess = sum(seconds) ;

            plant = floor(iSystemID/10) ;
            if plant==1
                plantname = 'WWTP1' ;
            else
                plantname = 'WWTP2' ;
            end
            switch iSystemID-plant*10
                case 0
                    plantcase = 'inert soluble' ;
                case 1
                    plantcase = 'reactive' ;
                case 2
                    plantcase = 'inert solid' ;
                case 3
                    plantcase = 'inert solid, customized' ;
            end

            Solutions = find(xLayoutOnFront)-1;
            [bubble_position,xExcess,iBubbleSize,NB2,nLayoutParetoExcess] = figPareto(criteria(Solutions+1,:),strProblem);
            drawnow()

            if nCriteria==2
                criteria = [ criteria zeros(nLayout,1) ];
            end

            iCountConcentrationSensor = nan(nLayout,1);
            for j=1:length(Solutions)
                iLayout0 = Solutions(j) ;
                xLayout0 = str2num(dec2bin(iLayout0,System.nSensorCandidate)');
                xSensorPlaced0 = System.xMeasured;
                xSensorPlaced0(System.linindexSensorCandidate(flipud(xLayout0==1))) = 1;
                iCountConcentrationSensor(iLayout0+1) = sum(xSensorPlaced0(:,1));
            end

            criteria_spread = [criteria iCountConcentrationSensor];
            criteria_select = criteria_spread(Solutions+1,:) ;

            [bubble_position,xExcess,iBubbleSize,NB1,nLayoutPareto] = fFigurePareto(criteria_select,strProblem);
            drawnow()
            figurepath	=	[	'.\figures\fig_Spread_Sys' num2str(iSystemID) '_Crit' num2str(nCriteria) '_Obj' strObjective	]   ;
            saveas (gcf,figurepath,'fig')
            saveas (gcf,figurepath,'epsc')
            saveas (gcf,figurepath,'tiff')


        end
    end

end

return

