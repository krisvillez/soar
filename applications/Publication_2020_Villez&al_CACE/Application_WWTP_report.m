clc
clear all
close all

verbose     =	0	;

addpath('../../graphtheory/')
addpath('../../soar/')
addpath('../../support/')
global OneComponent
global RedundancyDeficit

OneComponent = true ;
RedundancyDeficit = false ;

Count = 0 ;
iTabrow = 0;
MatrixOverview = cell(0);
for iSystemID	=  [ 10:12 20:23 ]
    for iObj = 1:2
        for nCriteria=2:3

            iTabrow  = iTabrow +1;

            lstCrit = 1:nCriteria;

            strProblem = ['Front_' num2str(iSystemID) '_' num2str(iObj)  '_' num2str(nCriteria)]	;

            switch iObj
                case 1
                    strObjective = 'FlowOnly';
                case 2
                    strObjective = 'FlowAndComponent1';
            end

            clear System
            clear Solutions
            clear iObjectives
            clear xObservable

            path1=['.\result\res_Sys' num2str(iSystemID) '_Crit' num2str(nCriteria) '_Obj' strObjective];
             load(path1 )
%             seconds(1) = time1-time0;

            Solutions = iTree(:,1);
            nLayoutChecked = sum(all(~isnan(criteria),2)) ;

            [bubble_position,xExcess,iBubbleSize,NB1,nLayoutPareto] = fFigurePareto(criteria(Solutions+1,:),strProblem);
            drawnow()

            load(['.\result\resext_Sys' num2str(iSystemID) '_Crit' num2str(nCriteria) '_Obj' strObjective] )
            nSensor = sum(System.xSensorCandidate(:)) ;
            nLayout = 2.^sum(System.xSensorCandidate(:)) ;

%            seconds(2) = time3;

            nLayoutCheckedExcess = sum(all(~isnan(criteria),2)) ;

            timeTotal = seconds(1);
            timeTotalExcess = sum(seconds) ;

            plant = floor(iSystemID/10) ;
            if plant==1
                plantname = 'WWTP1' ;
            else
                plantname = 'WWTP2' ;
            end
            switch iSystemID-plant*10
                case 0
                    plantcase = 'inert soluble' ;
                case 1
                    plantcase = 'reactive' ;
                case 2
                    plantcase = 'inert solid' ;
                case 3
                    plantcase = 'inert solid, customized' ;
            end

            [bubble_position,xExcess,iBubbleSize,NB2,nLayoutParetoExcess] = fFigurePareto(Front,strProblem);
            drawnow()
            figurepath	=	[	'.\figures\fig_Sys' num2str(iSystemID) '_Crit' num2str(nCriteria) '_Obj' strObjective	]   ;
            saveas (gcf,figurepath,'fig')
            saveas (gcf,figurepath,'epsc')
            saveas (gcf,figurepath,'tiff')

            input.data = [xExcess bubble_position iBubbleSize] ;
            input.dataFormat = {'%.0f',5}; % three digits precision for first two columns, one digit for the last
            input.tableColLabels = {'Excess redundancy? [0/1]','\# sensors','\# observable','\# redundant','\# layouts'};

            input.tableBorders = 1;
            input.tableCaption = ['\textbf{' plantname ' -- ' plantcase '.} Pareto front.' ];
            latex = latexTable(input);
            latex = char(latex) ;

            filename = ['.\tables\tab_front_Sys' num2str(iSystemID) '_Crit' num2str(nCriteria) '_Obj' strObjective '.tex'] ;
            dlmwrite(filename,latex,'');

%             try
%                 file = ['.\result\res_direct_Sys' num2str(iSystemID) '_Crit' num2str(nCriteria) '_Obj' strObjective] ;
%                 load(file)
%                 seconds(3) = time1-time0;
%                 Solutions = iTree(:,1);
%                 N3 = sum(all(~isnan(criteria),2)) ;
%                 [bubble_position,NB3,NF3] =            figPareto(criteria(Solutions+1,:),strProblem);
%                 %size(criteria)
%
%                 %N3 = sum(all(~isnan(criteria),2)) ;
%                 disp([ N3 ceil(N3/N0*1000)  NF3 ceil(NF3/N0*1000)  ])
%             catch
%                 seconds(3) =nan ;
%             end
%
%             index = Solutions(find(~ismember(Solutions+1,find(LayoutOnFront)))) ;
%             if isempty(index)
%                 disp('NO MISSING LAYOUTS')
%             else
%                 disp(['COUNTED ' num2str(length(index)) ' MISSING LAYOUTS'])
%             end
%
%              disp(ceil(seconds(:)'))
%              disp(ceil(sum(seconds(1:2))))
%              disp(ceil(sum(seconds(1:2))/3600*1000)/1000)
%             disp([ N2 ceil(N2/N0*1000)  NF2 ceil(NF2/N0*1000)  ])
%             disp([N0 N1 ceil(N1/N0*1000) NF1  ceil(NF1/N0*1000)  N2 ceil(N2/N0*1000)  NF2  ceil(NF2/N0*1000) ])

            MatrixOverview{iTabrow,1} = plantname;
                MatrixOverview{iTabrow,2} = plantcase ;
            MatrixOverview{iTabrow,3} = num2str(nSensor,'%d');
            MatrixOverview{iTabrow,4} = num2str(nLayout,'%d');
            MatrixOverview{iTabrow,5} = num2str(nLayoutChecked,'%d');
            MatrixOverview{iTabrow,6} = num2str(nLayoutChecked/nLayout*100,'%.1f');
            MatrixOverview{iTabrow,7} = num2str(nLayoutPareto,'%d');
            MatrixOverview{iTabrow,8} = num2str(nLayoutPareto/nLayout*100,'%.1f');
            MatrixOverview{iTabrow,9} = num2str(nLayoutCheckedExcess,'%d');
            MatrixOverview{iTabrow,10} = num2str(nLayoutCheckedExcess/nLayout*100,'%.1f');
            MatrixOverview{iTabrow,11} = num2str(nLayoutParetoExcess,'%d');
            MatrixOverview{iTabrow,12} = num2str(nLayoutParetoExcess/nLayout*100,'%.1f');
            if timeTotalExcess>3600*24
                MatrixOverview{iTabrow,13} = num2str(timeTotal/3600/24,'%.2f');
                MatrixOverview{iTabrow,14} = num2str(timeTotalExcess/3600/24,'%.2f');
                MatrixOverview{iTabrow,15} = 'd' ;
            elseif timeTotalExcess>3600
                MatrixOverview{iTabrow,13} = num2str(timeTotal/3600,'%.2f');
                MatrixOverview{iTabrow,14} = num2str(timeTotalExcess/3600,'%.2f');
                MatrixOverview{iTabrow,15} = 'h' ;
            else
%                MatrixOverview{iTabrow,13} = num2str(timeTotal,'%.2f');
%                MatrixOverview{iTabrow,14} = num2str(timeTotalExcess,'%.2f');
                MatrixOverview{iTabrow,15} = 's' ;
            end
        end
    end

end

input.data = MatrixOverview ;
input.dataFormat = {'%.0f',size(MatrixOverview,2)}; % three digits precision for first two columns, one digit for the last
input.tableColLabels = {'system','case','\#sensors','\#layouts',...
    '\#layouts checked','[\%]','\#Pareto layouts','[\%]',...
    '\#layouts checked','[\%]','\#Pareto layouts','[\%]',...
    'Step 1','Step 1+2',' ~ '};
input.tableBorders = 1;
input.tableCaption = ['Overview.' ];
latex = latexTable(input);
latex = char(latex) ;

filename = ['.\tables\tab_overview.tex'] ;
dlmwrite(filename,latex,'');


input.data = MatrixOverview(:,[1:6 9:10 13:15]) ;
input.dataFormat = {'%.0f',size(input.data,2)}; % three digits precision for first two columns, one digit for the last
input.tableColLabels = {'system','case','\#sensors','\#layouts',...
    '\#layouts checked','[\%]',...
    '\#layouts checked','[\%]',...
    'Step 1','Step 1+2',' ~ '};
input.tableBorders = 1;
input.tableCaption = ['Overview.' ];
latex = latexTable(input);
latex = char(latex) ;

filename = ['.\tables\tab_overview.tex'] ;
dlmwrite(filename,latex,'');
return

