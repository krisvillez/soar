
% -------------------------------------------------------------------------
% SOAR toolbox - Step1_ParetoSearch.m
%
% This script executes the search for the Pareto front of sensor layouts
% without surplus redundancy as in [1].
%
% References:
% [1] Villez, K.; Vanrolleghem, P. A.; Corominas, Ll. (2020). Pareto
% optimal placement of flow rate and concentration sensors in the bilinear
% case -- With application to wastewater treatment plants.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

clc
clear all
close all

verbose     =	5	;

addpath('../../graphtheory/')
addpath('../../soar/')
global OneComponent
global RedundancyDeficit

OneComponent = true ;

for iSearchMethod = 2

    switch iSearchMethod
        case 0 % search by exhaustive enumeration
            xExhaustive = true ;
            RedundancyDeficit = false ;
        case 1 % search with surplus redundanccy
            xExhaustive = false ;
            RedundancyDeficit = false ;
        case 2 % search without surplus redundanccy
            xExhaustive = false ;
            RedundancyDeficit = true ;
    end

    for iSystemID	= [ 10:12 20:23   ]
        for iObj=1:2
            for nCriteria=2:3

                strProblem = ['Front_' num2str(iSystemID) '_' num2str(iObj)  '_' num2str(nCriteria)]	;

                switch iObj
                    case 1
                        strObjective = 'FlowOnly';
                    case 2
                        strObjective = 'FlowAndComponent1';
                end

                tElapsedOuter = nan(5,1);

                tStartOuter = tic ;
                System          =	fDefineSystem(iSystemID,verbose)    ;
                tElapsedOuter(1) = toc(tStartOuter)                               ;

                System          =   fAddGraphInfo(System,verbose)       ;
                tElapsedOuter(2) = toc(tStartOuter)                               ;
                iNumberOfArc = System.iNumberOfArc ;

                switch strObjective
                    case 'FlowOnly'
                        System.xSensorCandidate(:,1:end-1) = 0;
                        System.rWeightObservability(:,1:end-1) = 0;
                        System.rWeightObservability(System.xArcPhysical,end) =1;
                    otherwise
                end

                nSensor = sum(System.xSensorCandidate(:));
                nLayout = 2^nSensor ;
                nRedundantMax =  nSensor;
                nObservableMax = sum(System.rWeightObservability(:)) ;

                [I,J]=find(System.xSensorCandidate);
                linindexSensorCandidate = sub2ind(size(System.xSensorCandidate),I,J) ;
                System.linindexSensorCandidate = linindexSensorCandidate ;
                System.nSensorCandidate = nSensor ;

                lookupTable = cell2mat(arrayfun(@(i)bitget([0:(2^System.nSensorCandidate-1)]',(System.nSensorCandidate+1)-i),1:System.nSensorCandidate,'UniformOutput',0));
                System.lookupTable = lookupTable;

                global criteria
                criteria               =	nan(nLayout,nCriteria)           ;

                global observableconfig
                observableconfig               =	nan(nLayout,(System.iNumberOfComponent+1)*2+1)           ;


                iLayoutBnd      =   [0 nLayout];

                % ====================================
                tElapsedOuter(3) = toc(tStartOuter)                               ;

                global lapse
                lapse               =	nan(nLayout,2)           ;

                if xExhaustive

                    for iLayout = iLayoutBnd(1):(iLayoutBnd(2)-1)
                        if mod(iLayout,2^6)==0
                            disp([iLayout iLayoutBnd(2)])
                        end
                        nCriterion =nCriteria;
                        fEvaluate(iLayout,System,nCriterion);
                    end
                    crituniq = unique(criteria,'rows') ;
                    crituniq(:,2:end) = -crituniq(:,2:end)  ;
                    xIncludeUniq = fParetoDominance(crituniq,crituniq);
                    critfront = crituniq(xIncludeUniq,:) ;
                    critfront(:,2:end) = -critfront(:,2:end)  ;

                    xLayoutOnFront = ismember(criteria,critfront,'rows');
                    Solutions = find(xLayoutOnFront)-1;
                    iObjectives = criteria(xLayoutOnFront,:) ;

                    tElapsedOuter(4) = toc(tStartOuter)                               ;

                else
                    fbnd        =	@(iLayoutInterval) fBound(iLayoutInterval,System,nCriteria);

                    iTree = fParetoSearch(fbnd,iLayoutBnd,100,100000,strProblem) ;
                    tElapsedOuter(4) = toc(tStartOuter)                               ;

                    iObjectives = iTree(:,2+(1:nCriteria)) ;
                    size(iObjectives)
                    iObjectives(:,2) = -iObjectives(:,2) ;
                    if nCriteria>=3
                        iObjectives(:,3) = -iObjectives(:,3) ;
                        if RedundancyDeficit
                            iObjectives(:,3) = iObjectives(:,3)+iObjectives(:,1);
                        end
                    end

                    Solutions = iTree(:,1);
                end

                fFigurePareto(iObjectives,strProblem);

                tElapsedOuter(2:4) = tElapsedOuter(2:4)-tElapsedOuter(1:3) ;

                save(['.\result\res_Sys' num2str(iSystemID) '_Crit' num2str(nCriteria) '_Obj' strObjective '_Search' num2str(iSearchMethod) '.mat'] );

            end
        end
    end
end

return

