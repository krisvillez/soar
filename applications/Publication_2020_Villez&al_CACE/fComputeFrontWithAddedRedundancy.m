function [criteria,xLayoutOnFront] = fComputeFrontWithAddedRedundancy(System,criteria,observableconfig,Solutions)

nCriteria = size(criteria,2) ;

lstCrit = 1:nCriteria;
xSensorCandidate = System.xSensorCandidate ;
binweight = 2.^(System.nSensorCandidate-(1:System.nSensorCandidate)') ;

xLayoutChecked = false(2^System.nSensorCandidate,1) ;
xLayoutOnFront = false(2^System.nSensorCandidate,1) ;
xLayoutOnFront(Solutions+1) = true;

Front = unique(criteria(Solutions+1,:),'rows') ;
SourceRidge = false;
if SourceRidge
    xFrontRidge = false(size(Front,1),1);
    for objObs = unique(Front(:,2))'
        select = find(Front(:,2)==objObs) ;
        select = select(Front(select,3)==max(Front(select,3)));
        xFrontRidge(select) = true;
    end
else
    xFrontRidge = true(size(Front,1),1);
end
FrontRidge = Front(xFrontRidge,:);

Ridge = Solutions(ismember(criteria(Solutions+1,:),FrontRidge,'rows'));
[~,index]=(sortrows(criteria(Ridge+1,:)));
Ridge = Ridge(index);
fFigurePareto(criteria(Ridge+1,:),'front');

nRidge = length(Ridge);

for iRidge = 1:nRidge

    %disp([ 'Row ' num2str(iRidge) ' of ' num2str(nRidge)])
    iLayout0 = Ridge(iRidge);
    crit0 = criteria(iLayout0+1,:);
    xLayout0 = str2num(dec2bin(iLayout0,System.nSensorCandidate)');
    xSensorPlaced0 = System.xMeasured;
    xSensorPlaced0(System.linindexSensorCandidate(flipud(xLayout0==1))) = 1;

    for j=1:size(System.rWeightObservability,2)
        xObservable(:,j) = str2num(dec2bin(observableconfig(iLayout0+1,j)-1,size(System.rWeightObservability,1))');
    end
    AddForRedundancy = all(cat(3,xSensorCandidate,~(xSensorPlaced0==1),xObservable),3);
    [I,J]=find(AddForRedundancy);
    nSensors2Add = length(I);
    for number = 1:nSensors2Add
        %disp([ 'Row ' num2str(iRidge) ' of ' num2str(nRidge) '   Config ' num2str(number) ' of ' num2str(nSensors2Add)])
        AddedSensorSets = nchoosek((1:length(I)),number);
        for iSet=1:size(AddedSensorSets,1)
            %                         xSensorPlaced0
            xSensorPlaced = xSensorPlaced0;
            for s = AddedSensorSets(iSet,:)
                i = I(s);
                j = J(s);
                xSensorPlaced(i,j) = 1;
            end
            xLayout1 = flipud(xSensorPlaced(System.linindexSensorCandidate)) ;
            iLayout1 = sum(binweight.*xLayout1(:)) ; %bin2dec(num2str(xLayout1(:)'));
            if xLayoutChecked(iLayout1+1)
                %                             disp('Skip')
            else
                xLayoutChecked(iLayout1+1) = true ;
                if xLayoutOnFront(iLayout1+1)
                    %disp('Already in set')
                else
                    crit1 = criteria(iLayout1+1,:) ;
                    if any(isnan(crit1))
                        crit1 = crit0 ;
                        crit1(1) = crit0(1)+ sum(xLayout1-xLayout0);
                        crit1(3) = crit0(3)+ sum(xLayout1-xLayout0);
                        criteria(iLayout1+1,:) = crit1;
                    end
                    nFront=size(Front,1);
                    Dominated = nan(nFront,nCriteria);
                    DominatedOrEqual = nan(nFront,nCriteria);
                    for iCrit=1:nCriteria
                        if iCrit==1
                            Dominated(:,iCrit) = crit1(iCrit)>Front(:,iCrit);
                            DominatedOrEqual(:,iCrit) = crit1(iCrit)>=Front(:,iCrit);
                        else
                            Dominated(:,iCrit) = crit1(iCrit)<Front(:,iCrit);
                            DominatedOrEqual(:,iCrit) = crit1(iCrit)<=Front(:,iCrit);
                        end

                    end

                    xInclude = true;
                    for iCrit=1:nCriteria
                        cols = lstCrit(lstCrit~=iCrit);
                        xChange = any(all(cat(2,DominatedOrEqual(:,cols),Dominated(:,iCrit) ),2),1);
                        if xChange
                            xInclude = false ;
                        end
                    end
                    if xInclude
                        %Count=Count+1;
                        xLayoutOnFront(iLayout1+1) = true;
                    else
                        %disp('Dominated layout')
                    end
                end
            end
        end
    end


end

nSolutions = length(xLayoutOnFront);
Front = criteria(xLayoutOnFront,:) ;

Bubbles = unique(criteria(xLayoutOnFront,:),'rows');
Bubbles(:,1) = -Bubbles(:,1);

nBubble=size(Bubbles,1);
for iBubble=1:nBubble
    crit_bubble = Bubbles(iBubble,:);
    crit_front = Bubbles(setdiff(1:nBubble,iBubble),:) ;
    DomOrEqual = crit_front>=ones(nBubble-1,1)*crit_bubble ;
    Dom = crit_front>ones(nBubble-1,1)*crit_bubble ;
    if any(and(all(DomOrEqual,2),any(Dom,2)))
        crit_point = crit_bubble;
        crit_point(1) = -crit_point(1);
        xLayoutOnFront(all(criteria==ones(nSolutions,1)*crit_point,2)) = false ;
    end
end


