clc
clear all
close all

verbose     =	0	;

addpath('../graphtheory/')
addpath('../soar/')
global OneComponent
global RedundancyDeficit

OneComponent = true ;
RedundancyDeficit = false ;

Count = 0 ;
for iSystemID	=  [ 10:12 20:23  ]
    for iObj = 1:2
        for nCriteria=2:3

            lstCrit = 1:nCriteria;

            strProblem = ['Front_' num2str(iSystemID) '_' num2str(iObj)  '_' num2str(nCriteria)]	;

            switch iObj
                case 1
                    strObjective = 'FlowOnly';
                case 2
                    strObjective = 'FlowAndComponent1';
            end

            clear System
            clear Solutions
            clear iObjectives
            clear xObservable

            load(['.\result\res_Sys' num2str(iSystemID) '_Crit' num2str(nCriteria) '_Obj' strObjective] )
            seconds(1) = time1-time0;

            N0 = 2.^sum(System.xSensorCandidate(:)) ;

            Solutions = iTree(:,1);
            N1 = sum(all(~isnan(criteria),2)) ;

            [bubble_position,NB1,NF1] = figPareto(criteria(Solutions+1,:),strProblem);
            drawnow()

            load(['.\result\resext_Sys' num2str(iSystemID) '_Crit' num2str(nCriteria) '_Obj' strObjective] )
            seconds(2) = time3;

            N2 = sum(all(~isnan(criteria),2)) ;

            [bubble_position,NB2,NF2] = figPareto(criteria(LayoutOnFront,:),strProblem);
            drawnow()
            try
                file = ['.\result\res_direct_Sys' num2str(iSystemID) '_Crit' num2str(nCriteria) '_Obj' strObjective] ;
                load(file)
                seconds(3) = time1-time0;
                Solutions = iTree(:,1);
                N3 = sum(all(~isnan(criteria),2)) ;
                [bubble_position,NB3,NF3] =            figPareto(criteria(Solutions+1,:),strProblem);
                %size(criteria)

                %N3 = sum(all(~isnan(criteria),2)) ;
                disp([ N3 ceil(N3/N0*1000)  NF3 ceil(NF3/N0*1000)  ])
            catch
                seconds(3) =nan ;
            end

            index = Solutions(find(~ismember(Solutions+1,find(LayoutOnFront)))) ;
            if isempty(index)
                disp('NO MISSING LAYOUTS')
            else
                disp(['COUNTED ' num2str(length(index)) ' MISSING LAYOUTS'])
            end
%
             disp(ceil(seconds(:)'))
             disp(ceil(sum(seconds(1:2))))
             disp(ceil(sum(seconds(1:2))/3600*1000)/1000)
            disp([ N2 ceil(N2/N0*1000)  NF2 ceil(NF2/N0*1000)  ])
            disp([N0 N1 ceil(N1/N0*1000) NF1  ceil(NF1/N0*1000)  N2 ceil(N2/N0*1000)  NF2  ceil(NF2/N0*1000) ])

        end
    end

end

return

