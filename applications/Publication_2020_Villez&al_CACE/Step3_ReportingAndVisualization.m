
% -------------------------------------------------------------------------
% SOAR toolbox - Step3_ReportingAndVisualization.m
%
% This script produces tables and figures describing the Pareto front and
% the computational requirements to obtain it, as reported in [1].
%
% References:
% [1] Villez, K.; Vanrolleghem, P. A.; Corominas, Ll. (2020). Pareto
% optimal placement of flow rate and concentration sensors in the bilinear
% case -- With application to wastewater treatment plants.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

clc
clear all
close all

verbose     =	0	;

addpath('../../graphtheory/')
addpath('../../soar/')
global OneComponent
global RedundancyDeficit

OneComponent = true ;
RedundancyDeficit = true ;


for iSearchMethod = 2
    for iSystemID	= [ 10:12 20:23   ]
        for iObj=1:2
            for nCriteria=2:3

                switch iObj
                    case 1
                        strObjective = 'FlowOnly';
                    case 2
                        strObjective = 'FlowAndComponent1';
                end
                strProblem = ['Front_' num2str(iSystemID) '_' num2str(iObj)  '_' num2str(nCriteria)]	;

                filename = ['.\result\res_Sys' num2str(iSystemID) '_Crit' num2str(nCriteria) '_Obj' strObjective '_Search' num2str(iSearchMethod) '_ext' '.mat'] ;
                load( filename )

                deltaT = sum(tElapsedOuter);

                GenobsT =sum(lapse(~isnan(lapse(:,2)),2));
                GenobsTmn =mean(lapse(~isnan(lapse(:,2)),2));

                if nCriteria>=3
                    GenredT =sum(lapse(~isnan(lapse(:,3)),3));
                    GenredTmn =mean(lapse(~isnan(lapse(:,3)),3));
                end

                Front = criteria(xLayoutOnFront,:) ;
                nConsidered = sum(~isnan(criteria(:,1))) ;
                nOptimal = size(Front,1);

                disp('%==============================')
                disp(['System: ' num2str(iSystemID)])
                disp([' Sensor types: ' strObjective])
                disp([' Criteria: ' num2str(nCriteria)])
                disp([' Total time [hh:mm:ss.fff]: ' datestr((datenum(0,0,0,0,0,deltaT)),'HH:MM:SS.FFF')])
                disp([' GENOBS time [hh:mm:ss.fff]: ' datestr((datenum(0,0,0,0,0,GenobsT)),'HH:MM:SS.FFF')])

                if nCriteria>=3
                    disp([' GENRED time [hh:mm:ss.fff]: ' datestr((datenum(0,0,0,0,0,GenredT)),'HH:MM:SS.FFF')])
                end
                disp([' GENOBS time per layout [ms]: ' num2str(GenobsTmn*1000,'%07.3f') ])
                if nCriteria>=3
                    disp([' GENRED time per layout [ms]: ' num2str(GenredTmn*1000,'%07.3f') ])
                end
                disp('%==============================')
                disp([' Layouts: ' num2str(nLayout) ])
                disp([' Layouts considered: ' num2str(nConsidered) ])
                disp([' Layouts omitted: ' num2str(nLayout-nConsidered) ])
                disp([' Layouts optimal: ' num2str(nOptimal) ])
                disp('%==============================')


                Front = criteria(xLayoutOnFront,:) ;
                [bubble_position,NB1,NF1] = fFigurePareto(Front(:,1:nCriteria),strProblem);
                drawnow()
                figname = ['./figure/WWTP' num2str(iSystemID) '_' num2str(iObj)  '_' num2str(nCriteria) '_ParetoSet_Front' ]	;
                saveas(gcf, figname, 'epsc')

                if iObj==2
                    if nCriteria==2
                        criteria = [ criteria zeros(nLayout,1) ];
                    end

                    iCountConcentrationSensor = nan(nLayout,1);
                    Solutions = find(xLayoutOnFront)-1;
                    for j=1:length(Solutions)
                        iLayout0 = Solutions(j) ;
                        xLayout0 = str2num(dec2bin(iLayout0,System.nSensorCandidate)');
                        xSensorPlaced0 = System.xMeasured;
                        xSensorPlaced0(System.linindexSensorCandidate(flipud(xLayout0==1))) = 1;
                        iCountConcentrationSensor(iLayout0+1) = sum(xSensorPlaced0(:,1));
                    end

                    criteria_spread = [criteria iCountConcentrationSensor];
                    criteria_select = criteria_spread(Solutions+1,:) ;

                    [bubble_position,xExcess,iBubbleSize,NB1,nLayoutPareto] = fFigurePareto(criteria_select,[strProblem '_mix'] );
                    drawnow()
                    figname = ['./figure/WWTP' num2str(iSystemID) '_' num2str(iObj)  '_' num2str(nCriteria) '_ParetoSet_withSensorTypeFraction' ]	;
                    saveas(gcf, figname, 'epsc')
                end
            end
        end

    end

end

return

