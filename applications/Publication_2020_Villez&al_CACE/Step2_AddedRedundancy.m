
% -------------------------------------------------------------------------
% SOAR toolbox - Step2_AddedRedundancy.m
%
% This script executes the search for the complete Pareto front based on
% the pre-computed Pareto front of sensor layouts without surplus,
% redundancy as in [1].
%
% References:
% [1] Villez, K.; Vanrolleghem, P. A.; Corominas, Ll. (2020). Pareto
% optimal placement of flow rate and concentration sensors in the bilinear
% case -- With application to wastewater treatment plants.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2019-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

clc
clear all
close all

verbose     =	0	;

addpath('../../graphtheory/')
addpath('../../soar/')
global OneComponent
global RedundancyDeficit

OneComponent = true ;

for iSearchMethod = 2
    for iSystemID	= [ 10:12 20:23   ]
        for iObj=1:2
            for nCriteria=2:3

                strProblem = ['Front_' num2str(iSystemID) '_' num2str(iObj)  '_' num2str(nCriteria)]	;

                switch iObj
                    case 1
                        strObjective = 'FlowOnly';
                    case 2
                        strObjective = 'FlowAndComponent1';
                end

                clear System
                clear Solutions
                clear iObjectives
                clear xObservable

                load(['.\result\res_Sys' num2str(iSystemID) '_Crit' num2str(nCriteria) '_Obj' strObjective '_Search' num2str(iSearchMethod)  '.mat']  )

                tStartOuter = tic ;

                fFigurePareto(criteria(Solutions+1,:),strProblem);
                drawnow()

                if nCriteria==2
                    criteria = [ criteria zeros(nLayout,1) ];
                end

                if nCriteria>2 && RedundancyDeficit
                    [criteria,xLayoutOnFront] = fComputeFrontWithAddedRedundancy(System,criteria,observableconfig,Solutions) ;
                else
                    xLayoutOnFront = ismember(1:size(criteria,1),Solutions+1) ;
                end

                tElapsedOuter(end) = toc(tStartOuter)                               ;

                Front = criteria(xLayoutOnFront,:) ;
                fFigurePareto(Front(:,1:nCriteria),strProblem);
                drawnow()

                filename = ['.\result\res_Sys' num2str(iSystemID) '_Crit' num2str(nCriteria) '_Obj' strObjective '_Search' num2str(iSearchMethod) '_ext'  '.mat']
                save(filename  )
                disp('done')
            end
        end

    end

end

return

