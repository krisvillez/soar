To reproduce the results obtained in [2-3], execute the following in the Matlab command window:

	Demo_KM1988
	
	
References
[2] Kretsovalis, A. and Mah, R. S. H. (1988a). Observability and redundancy classification in generalized process networks - I. Theorems. Comp. Chem. Eng., 12(7), 671-687.
[3] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and redundancy classification in generalized process networks - II. Algorithms. Comp. Chem. Eng., 12(7), 689-703.   


----------------------------------------------------------
Last modified on 27 January 2016