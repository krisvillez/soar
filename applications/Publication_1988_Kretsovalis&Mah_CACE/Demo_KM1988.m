
% -------------------------------------------------------------------------
% SOAR toolbox - Demo_KM1988.m
%
% This script executes the GENOBS and GENRED algorithm as provided by
% [2-3].
%
% References:
% [2] Kretsovalis, A. and Mah, R. S. H. (1988a). Observability and
% redundancy classification in generalized process networks - I. Theorems.
% Comp. Chem. Eng., 12(7), 671-687.
% [3] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and
% redundancy classification in generalized process networks - II.
% Algorithms. Comp. Chem. Eng., 12(7), 689-703.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2013-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

clc
clear all
close all

iSystemID = 3

% add necessary paths:
addpath('../../graphtheory/')
addpath('../../soar/')

% true if results need to be compared with results reported in articles
truth       =   true    ;

% verbose levels 0 (no output) - 5 (maximum output)
verbose     =   5       ;

tic

System          =   fDefineSystem(iSystemID,verbose)    ;
System          =   fAddGraphInfo(System,verbose)       ;

xObservable     =   fGenObs(System,verbose)             ;
xRedundant      =   fGenRed(System,verbose)             ;

if truth
  disp(sprintf(['\n']))
  disp(sprintf(['VERIFYING RESULTS \n']))

  % True observability classification as reported in paper:
  ObsTruth = System.reference.xObservable;

  xObservable(isnan(xObservable)) = -2;
  ObsTruth(isnan(ObsTruth)) = -2;
  [I,J]       =   find(xObservable~=ObsTruth)	;
  if isempty(I)
    disp(sprintf(['GENOBS: No inconsistencies \n']))
  else
    disp(sprintf(['GENOBS: Inconsistencies: ']))
    disp([I J-1])
    disp(sprintf(['\n']))
  end

  % True observability classification as reported in paper:
  RedTruth = System.reference.xRedundant;

  xRedundant(isnan(xRedundant)) = -2;
  RedTruth(isnan(RedTruth)) = -2;
  [I,J]       =   find(xRedundant~=RedTruth)	;
  if isempty(I)
    disp(sprintf(['GENRED: No inconsistencies \n']))
  else
    disp(sprintf(['GENRED: Inconsistencies: ']))
    disp([I J-1])
    disp(sprintf(['\n']))
  end
end

disp(sprintf([  'End of test \n' repmat('=',1,42) '\n']))

time=toc;

disp(sprintf([  'Total time taken tests [s]: ' num2str(time,'%.2f') ' \n' repmat('=',1,42) '\n']))
return
