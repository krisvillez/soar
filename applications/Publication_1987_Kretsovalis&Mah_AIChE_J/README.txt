To reproduce the results obtained in [1], execute the following in the Matlab command window:

	Demo_KM1987
	
	
References
[1] Kretsovalis, A. and Mah, R. S. H. (1987). Observability and redundancy classification in multicomponent process networks. AIChE Journal, 33(1), 70-82.  


----------------------------------------------------------
Last modified on 27 January 2016