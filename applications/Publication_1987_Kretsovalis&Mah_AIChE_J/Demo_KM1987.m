
% -------------------------------------------------------------------------
% SOAR toolbox - Demo_KM1987.m
%
% This script executs the observability and redundancy classification
% algorithms as provided by [1].
%
% References:
% [1] Kretsovalis, A. and Mah, R. S. H. (1987). Observability and
% redundancy classification in multicomponent process networks. AIChE
% Journal, 33(1), 70-82.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2023-04-06
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2013-2023 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave.
%
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
%
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

clc
clear all
close all

iSystemID  = 1

% add necessary paths:
addpath('../../graphtheory/')
addpath('../../soar/')

% true if results need to be compared with results reported in articles
truth       =   true    ;

% verbose levels 0 (no output) - 5 (maximum output)
verbose     =   5       ;

tic

System          =	fDefineSystem(iSystemID,verbose)    ;
System          = fAddGraphInfo(System,verbose)       ;

xObservable     =	fGenObs(System,verbose)             ;
xRedundant      =	fGenRed(System,verbose)             ;

time=toc;

disp(sprintf([  'Total time taken for tests [s]: ' num2str(time,'%.2f') ' \n' repmat('=',1,42) '\n']))

return
