SOAR toolbox v2.0 - Structural Observability and Redundancy toolbox
by Kris Villez

Website:	http://homepages.eawag.ch/~villezkr/spike/
Contact: 	kris.villez@eawag.ch

This toolbox is a collection of Matlab/Octave optimization functions and scripts for structural observability and structural redundancy labelling and optimization. It is self-contained.

These files are provided as is, with no guarantees and are intended for non-commercial use. These routines have been developed and tested successfully with Matlab (R2014b, R2015b, R2016b, R2019a) and Octave (v3.6.4) on a Windows system. To install the Matlab functions in a Windows systems, using Matlab or Octave, follow the instructions below.

---------------------------
 I. Installing the toolbox
---------------------------

1. Create a directory to contain the Matlab functions.  For example, in my case the directory is

		C:\Tools\SOAR

2. Unzip the contents of the zip-file into this directory.



-----------------------
 II. Using the toolbox
-----------------------

1. Start Matlab

2. To enable access to the functions in the SOAR toolbox from any other folder, add the toolbox to the Matlab/Octave path by typing the following in the command line

		addpath(genpath('C:\Tools\SOAR'))

This allows access during the current Matlab/Octave session.

3. To enable access to the SPIKE_O toolbox permanently (across sessions), type the following in the command line:
	
		savepath

4. For help with the SOAR toolbox, type 'help FUN' (without quotes) and FUN the actual function you want help with, e.g.:
	
		help GENOBS

5. Practical use cases of the toolbox are available in the subfolder 'SOAR_Applications'. These folder includes scripts to reproduce the results in publications [1-4].
	


----------------------------
 III. Relevant publications
----------------------------

The following publications inspired this work:
[1] Kretsovalis, A. and Mah, R. S. H. (1987). Observability and redundancy classification in multicomponent process networks. AIChE Journal, 33(1), 70-82.  
[2] Kretsovalis, A. and Mah, R. S. H. (1988a). Observability and redundancy classification in generalized process networks - I. Theorems. Comp. Chem. Eng., 12(7), 671-687.
[3] Kretsovalis, A. and Mah, R. S. H. (1988b). Observability and redundancy classification in generalized process networks - II. Algorithms. Comp. Chem. Eng., 12(7), 689-703.

Please refer to this toolbox by citing one or more of the following publications:
[4] Villez, K, Vanrolleghem, P A, Corominas, L (2016). Optimal flow sensor placement on wastewater treatment plants. Wat Res, 101, 75-83. DOI: https://doi.org/10.1016/j.watres.2016.05.068 
[5] Villez, K, Vanrolleghem, P A, Corominas, L (2020). Optimal placement of flow rate and water quality sensors on wastewater treatment plants.  Comp. Chem. Eng., 139, 106880. DOI: https://doi.org/10.1016/j.compchemeng.2020.106880


-------------
 IV. Changes
-------------

- Updated code to facilitate comparison of computational requirements with SOAR v2.0.

------------------
 Acknowledgements
------------------

I want to thank the following people for code included in this toolbox:
- Juan Pablo Carbajal
- Eli Duenisch
- Pascal Getreuer
- Timothy E. Holy
- Sergii Iglin
- Will Robertson

----------------------------------------------------------
Last modified on 26 December 2019